<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Song;
use App\Content;
use App\Stats;
use Illuminate\Http\Request;
use wapmorgan\Mp3Info\Mp3Info;
use Storage;
use Illuminate\Contracts\Filesystem\Filesystem;
use DB;
use App\User;

class SongsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
       // $songs = Song::orderBy('id', 'DESC')->paginate(10);;
        $songs = DB::table('affirmation')->where('status',1)->groupBy('affirmation_title')->orderBy('id', 'DESC')->get();

        // dd($songs);

        return view('admin.songs.index', compact('songs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
       // $artists = Content::where(['cat_id'=>'1'])->get();
        $artists = DB::table('voice')->get();

        $albums = Content::where(['cat_id'=>'2'])->get();

        $playlists = Content::where(['cat_id'=>'3'])->get();
        $tracks = Content::where(['cat_id'=>'4'])->get();

        return view('admin.songs.create', compact('artists', 'albums', 'playlists', 'tracks'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        //$requestData = $request->all();
        $image = "";
        $song_id = "";
        $duration = "";
        $songFile = "";

        /*$selectedArtist = $requestData['selectedArtist'];
        $selectedAlbum = $requestData['selectedAlbum'];*/
        // $selectedPlaylist = $requestData['selectedPlaylist'];
        // $selectedTrack = $requestData['selectedTrack'];

        /*if ($selectedArtist) {

            $contents_data[] = $selectedArtist;
        }
        if ($selectedAlbum) {

            $contents_data[] = $selectedAlbum;
        }*/
        // if ($selectedPlaylist) {

        //     $contents_data[] = $selectedPlaylist;
        // }
        // if ($selectedTrack) {

        //     $contents_data[] = implode(',', $selectedTrack);
        // }

        /*if (!empty($contents_data)) {
            $contents_ids = implode(',', $contents_data);
        }*/


        if($request->file('image')){
            $image = $this->uploadS3Data(@$request->file('image'), 'songPic');
        }

        if($request->file('song')){

            
          for($i=0;$i< sizeof($request->selectedArtist); $i++){ 

            $audio = new Mp3Info($request->file('song')[$i], true);
            $duration = (int) ($audio->duration)*1000; // duration in milliseconds
            $songFile = $this->uploadS3Data(@$request->file('song')[$i], 'songs');

            $data[]=array(
                            'cat_id'=>$request->selectedAlbum,
                            'artist_id'=>$request->selectedArtist[$i],
                            'affirmation_songs'=>$songFile,
                            'affirmation_images'=>$image,
                            'affirmation_duration'=>$duration,
                            'affirmation_title'=>$request->name,
                            'affirmation_subtitle'=>$request->subtitle,
                            'affirmation_discription'=>$request->description,
                            'free_premium'=>$request->plans
                           );
        }
           

         $song=   DB::table('affirmation')->insert($data);

         if ($song) {
                $payload = [];
                $payload = [
                    'title' =>$request->name, 
                    'body'  => $request->description, 
                    'value' => '',
                    'icon' =>asset('public/img/admin/fb.png'),    
                    'sound' => 'default', 
                    'priority' => 'high',
                    'type'  => 2
                ];
              $mydata=   $this->sendNotification($user = [], $payload);


              $arrays=array(
                            'title'=>$request->name,
                            'body'=>$request->description,
                            'icon'=>asset('public/img/admin/fb.png'),
                            'sound' => 'default', 
                            'priority' => 'high',
                            'type'  => 2
                            );

              $notificationdata['type']='admin';
              $notificationdata['nofitication_type']=2;
              $notificationdata['message']=json_encode($arrays);
              DB::table('notifications')->insert($notificationdata);
              /*print_r($mydata);
              die();*/
          }
            
            /*$data = [
                'content_id' => $contents_ids,
                'image'      => $image,
                'song'       => $songFile,
                'duration'   => $duration,
                'name'       => $requestData['name'],
                'subtitle'   => $requestData['subtitle'],
                'description'=> $requestData['description'],
            ];

            $song = Song::create($data);
            if ($song) {
                $payload = [];
                $payload = [
                    'title' =>'Add new song', 
                    'body'  => 'New song added', 
                    'value' => '', 
                    'type'  => 2
                ];*/
                
                
            }
            return redirect('admin/songs')->with('flash_message', 'Song added!');

        }

    //}


        public function mynotification(){
            $token=array();
            $array=User::select('device_token')->get();
            foreach($array as $ft){
                if($ft->device_token!='' || $ft->device_token!= NULL){
                $device_token=$ft->device_token;
                array_push($token,$device_token);
                }
            }
                // print_r($token); die();
                 define('API_ACCESS_KEY','AAAA8ylrkbg:APA91bHsXYpoZDDAqinIXOJPS2fiDnBqvxzzuFQq6tjWARWpMIBXnvXBAwQ5gV2JtC_-FqPUynpd3gRG8IeMc8PmO9Ps9x-aF48r4asT8LQtT8d-h3q4RzXpVDnTVAbXuMcruWsc-d_C');
                $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
                $notification = [
                'title' =>'My new Songs',
                'body' => 'Meditation App notification',
                'icon' =>'http://localhost/notification/notefication.png',  
                'sound' => 'default'
                ];
                $fcmNotification = [
                'registration_ids' => $token, //multple token array
                'notification' => $notification,
                'title' => 'Hellow words',
                'data' => $notification,
                'priority' => 'high',
                ];
                $headers = [
                'Authorization: key='.API_ACCESS_KEY,
                'Content-Type: application/json'
                ];
                
        
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,$fcmUrl);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
                $result = curl_exec($ch);
                curl_close($ch);
                print_r($result);

           
        }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $song = DB::table('affirmation')->where('id',$id)->first();

        return view('admin.songs.show', compact('song'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $song = DB::table('affirmation')->where('id',$id)->first();

        $artists    =  DB::table('voice')->get();
        $albums     = Content::where(['cat_id'=>'2'])->get();
        $playlists  = Content::where(['cat_id'=>'3'])->get();
        $tracks     = Content::where(['cat_id'=>'4'])->get();

       /* $getArtist      = Song::where(['id' => $id ])->first(['content_id']);
        $getAlbum       = Song::where(['id' => $id ])->first(['content_id']);
        $getPlaylist    = Song::where(['id' => $id ])->first(['content_id']);
        $getTrack       = Song::where(['id' => $id ])->first(['content_id']);*/




        return view('admin.songs.edit', compact('song', 'artists', 'albums', 'playlists', 'tracks'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        

        if($request->file('image')){
            $image = $this->uploadS3Data(@$request->file('image'), 'songPic');
            $data['affirmation_images']= $image ;
        }
        
         
          for($i=0;$i< sizeof($request->selectedArtist); $i++){ 

             if(isset($request->file('song')[$i]) && !empty($request->file('song')[$i]))
                  {
                    print_r($request->file('song')[$i]);

                    $audio = new Mp3Info($request->file('song')[$i], true);
                    $duration = (int) ($audio->duration)*1000; // duration in milliseconds
                    $songFile = $this->uploadS3Data(@$request->file('song')[$i], 'songs');
                     $data['affirmation_songs']= $songFile ;
                     $data['affirmation_duration']= $duration ;
                  }

                            $data['cat_id']=$request->selectedAlbum;
                            $data['artist_id']=$request->selectedArtist[$i];
                            $data['affirmation_title']=$request->name;
                            $data['affirmation_subtitle']=$request->subtitle;
                            $data['affirmation_discription']=$request->description;
                            $data['free_premium']=$request->plans;

         if(DB::table('affirmation')->where('affirmation_title',$request->name)->where('affirmation_subtitle',$request->subtitle)->where('artist_id',$request->selectedArtist[$i])->where('cat_id',$request->selectedAlbum)->count('id') > 0){
                   DB::table('affirmation')->where('id',$request->affirmation_id[$i])->update($data);
              }else{
                 DB::table('affirmation')->insert($data);
              }
         
        }
        return redirect('admin/songs')->with('flash_message', 'Song updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        DB::enableQueryLog();
        DB::table('affirmation')->where('id',$id)->update(array('status'=>0));
        //dd(DB::getQueryLog());

        return redirect('admin/songs')->with('flash_message', 'Song deleted!');
    }

    public static function songPlayCount($id) {
        $get_data = Stats::selectRaw('sum(stats.play_count) as play_count, song_id')->where('song_id', $id)->groupBy('song_id')->orderBy('play_count', 'DESC')->get();
        if (!empty($get_data)) {

            return (int) @$get_data[0]->play_count;
        } else{

            return 0;
        }

    }

   public static function getSongsByUser($title,$SubTitle){
     return $response=DB::table('affirmation')->where('affirmation_title',$title)->where('affirmation_subtitle',$SubTitle)->get();
    }



}
