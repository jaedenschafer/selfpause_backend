@extends('layouts.admin')

@section('content')

 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

<div class="container-fluid">

    <div class="col-md-12">

            <br />

        @if($message = Session::get('message'))

            <div class="alert alert-warning alert-block">

            <button type="button" class="close" data-dismiss="alert">×</button>

            <strong>{{ $message }}</strong>

            </div>

            @endif

     <a href="{{ url('/admin/nature') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>

     <br />

     <br />

     <div class="card">

        <div class="card-header">Create New Sounds</div>

        <div class="card-body">

                <form method="POST" action="{{ url('/admin/natureSave') }}" accept-charset="UTF-8" enctype="multipart/form-data">

                    @csrf

                    <div class="form-group">

                         <label>Enter Name</label>

                        <input type="text" name="name" id="name" class="form-control" placeholder="Enter Name" required="" value="{{@$records->nature_name}}">



                    </div>
                    <!-- <div class="form-group">

                        <label> Price </label>

                            <input type="number" name="price" id="price"  class="form-control" value="{{@$records->price}}">

                        </div> -->
                    <input type="hidden" name="voiceid" value="{{@$records->nature_id}}">

                     <div class="form-group">

                        <label> Unlock Choose images </label>

                            <input type="file" name="images" id="images"  class="form-control">

                        </div>

                        <div class="form-group">

                        <label> Locked Choose images </label>

                            <input type="file" name="LockedImages" id="LockedImages"  class="form-control">

                        </div>
                        <div class="form-group">

                        <label> Icon </label>

                            <input type="file" name="icon" id="icon"  class="form-control">

                        </div>

                         <div class="form-group">

                             <label> Choose nature sound </label>



                            <input type="file" name="voice" id="voice"  class="form-control">

                        </div>
                         <div class="form-group">

                             <label> Select type </label>
                             <select class="form-control" name="nature_type">
                                 <option value="">Select type</option>
                                 <option value="Nature" @if(@$records->music_type=='Nature') {{'selected'}} @endif >Nature</option>
                                 <option value="Music" @if(@$records->music_type=='Music') {{'selected'}} @endif >Music</option>
                                  <option value="Session" @if(@$records->music_type=='Session') {{'selected'}} @endif >Session</option>
                             </select>

                        </div>
                                <div class="form-group">
                                <label>Free </label>
                                <input type="radio" name="plans"  placeholder="Free" value="0" @if(@$records->free_premium==0) {{'checked'}} @endif>
                                <label>Premium</label>
                                <input type="radio" name="plans" placeholder="Premium" value="1"  @if(@$records->free_premium==1) {{'checked'}} @endif>
                                </div>
                         <div class="form-group">

                            <input type="submit" name="submit" class="btn btn-primary sm-btn" value="Submit">

                        </div>

                </form>

        </div>

    </div>

</div>



</div>







@endsection

