<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use PHPMailer\PHPMailer;
use wapmorgan\Mp3Info\Mp3Info;
use App\User;
use App\Content;
use App\Song;
use App\Stats;
use App\Favourite;
use App\UserPlaylist;
use App\Banner;
use App\FeaturedPlaylist;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Response;
use Auth;
use Mail;
use URL;
use DB;
use File;
use Input;
use Storage;
use Illuminate\Contracts\Filesystem\Filesystem;

class CollectionsController extends Controller
{

	/**
     * Get Home all data
     *
     * @param  user_id
     * @return response
     */
	public function getHomeData(Request $request)
	{

		$this->validation($request->all(), [
			"user_id" => "required",
		]);

		$userinfo =  User::where('id',$request->input('user_id'))->first();
		if (empty($userinfo)) {
			$this->error('User does not exist.', []);
		}

		//1=> artist, 2=> album, 3=> playlist, 4=> track
		$banner_data = $this->getBannerData($request->input('user_id'), 3, 5);

		$song_data = $this->getSongData($request->input('user_id'), 2, 10);

		$albums_data = $this->getContentsData($request->input('user_id'), 2, 5);

		$popular_playlist_data = $this->getPopularPlaylistBySong($request->input('user_id'), 3, 5);

		$featured_playlist_data = $this->getFeaturedPlaylistData($request->input('user_id'), 3, 5);

		$response = [];
		$response['banner_data'] = $banner_data;
		$response['song_data'] = $song_data;
		$response['album_data'] = $albums_data;
		$response['popular_playlist_data'] = $popular_playlist_data;
		$response['featured_playlist_data'] = $featured_playlist_data;

		if($response)
		{
			$this->success('record found.', $response);
		}else{
			$this->error('record not fount', []);
		}
	}

	/**
     * Get Home all data
     *
     * @param  user_id
     * @return response
     */
	public function getExploreData(Request $request)
	{
		$this->validation($request->all(), [
			"user_id" => "required",
		]);

		$userinfo =  User::where('id',$request->input('user_id'))->first();
		if (empty($userinfo)) {
			$this->error('User does not exist.', []);
		}

		//1=> artist, 2=> album, 3=> playlist, 4=> track
		$featured_playlist_data = $this->getFeaturedPlaylistData($request->input('user_id'), 3, 5);

		$genre_moods = $this->getContentsData($request->input('user_id'), 4, 10);

		$albums_data = $this->getContentsData($request->input('user_id'), 3, 5);

		$artist_data = $this->getContentsData($request->input('user_id'), 1, 5);


		$response = [];
		$response['featured_playlist_data'] = $featured_playlist_data;
		$response['genre_moods'] = $genre_moods;
		$response['album_data'] = $albums_data;
		$response['artist_data'] = $artist_data;

		if($response)
		{
			$this->success('record found.', $response);
		}else{
			$this->error('record not fount', []);
		}
	}

	/**
     * Get Genres and mood
     *
     * @param  user_id
     * @return response
     */
	public function getGenresMood(Request $request)
	{
		$this->validation($request->all(), [
			"user_id" => "required",
		]);

		$userinfo =  User::where('id',$request->input('user_id'))->first();
		if (empty($userinfo)) {
			$this->error('User does not exist.', []);
		}

		$per_page = 20;

		if ($request->input('page') == "") {
			$skip = 0;
		} else {
			$skip = $per_page * $request->input('page');
		}

		if ($request->input('page') == '') {
			$take = 20;
		}else{
			$take = ( (int) @$request->input('page') + 2) * 10;
		}

		//1=> artist, 2=> album, 3=> playlist, 4=> genre_mood

		$file_path = $this->getPath(4);
		$results = Content::where(['cat_id' => 4])->orderBy('id', 'DESC')->skip($skip)->take($take)->get();
		$response = [];
		foreach ($results as $key => $value) {
			$response[] = [
				'id' => $value->id,
				'category_id' => $value->cat_id,
				'name' => (string) $value->name,
				'subtitle' => (string) $value->subtitle,
				'description' => (string) $value->description,
				'file_image' => $this->getFilePath($value->image, $file_path),
				'is_favourite' => $this->isFavourite($request->input('user_id'), $value->id, 4),
				'created_at' => date('Y-m-d H:i:s', strtotime($value->created_at)),
			];
		}

		if($response)
		{
			$this->success('record found.', $response);
		}else{
			$this->error('record not fount', []);
		}
	}

	/**
     * Explore screen (New Tab)
     *
     * @param  user_id
     * @return response
     */
	public function getExploreNewInfo(Request $request)
	{
		$this->validation($request->all(), [
			"user_id" => "required",
		]);

		$userinfo =  User::where('id',$request->input('user_id'))->first();
		if (empty($userinfo)) {
			$this->error('User does not exist.', []);
		}

		//1=> artist, 2=> album, 3=> playlist, 4=> track
		$song_data = $this->getSongData($request->input('user_id'), 2, 5);

		$albums_data = $this->getContentsData($request->input('user_id'), 2, 5);

		$response = [];
		$response['song_data'] = $song_data;
		$response['album_data'] = $albums_data;

		if($response)
		{
			$this->success('record found.', $response);
		}else{
			$this->error('record not fount', []);
		}
	}

	/**
     * Explore screen (Top Tab)
     *
     * @param  user_id
     * @return response
     */
	public function getExploreTopInfo(Request $request)
	{
		$this->validation($request->all(), [
			"user_id" => "required",
		]);

		$userinfo =  User::where('id',$request->input('user_id'))->first();
		if (empty($userinfo)) {
			$this->error('User does not exist.', []);
		}

		//1=> artist, 2=> album, 3=> playlist, 4=> track
		$song_data = $this->getPopularSongByStats($request->input('user_id'), 2, 10);

		$albums_data = $this->getContentsData($request->input('user_id'), 2, 10);

		$response = [];
		$response['song_data'] = $song_data;
		$response['album_data'] = $albums_data;

		if($response)
		{
			$this->success('record found.', $response);
		}else{
			$this->error('record not fount', []);
		}
	}

	/**
     * get contents data acording to category
     *
     * @param  user_id, category_id, take
     * @param  Reference of category: 1=> artist, 2=> album, 3=> playlist, 4=> songs
     * @return response
     */
	public function getContentsData($user_id = '', $category_id = 1, $take = 5, $page = 1) {

		$skip = 0;
		$skip = $take * ($page - 1);

		$file_path = $this->getPath($category_id);
		$results = Content::where(['cat_id' => $category_id])->orderBy('id', 'DESC')->skip($skip)->take($take)->get();
		$response = [];
		foreach ($results as $key => $value) {
			$response[] = [
				'id' => $value->id,
				'category_id' => $value->cat_id,
				'name' => (string) $value->name,
				'subtitle' => (string) $value->subtitle,
				'description' => (string) $value->description,
				'file_image' => $this->getFilePath($value->image, $file_path),
				'is_favourite' => $this->isFavourite($user_id, $value->id, $category_id),
				'created_at' => date('Y-m-d H:i:s', strtotime($value->created_at)),
			];
		}
		return $response;
	}


	public function setContentsInfo(Request $request, $category_id = 1, $take = 5, $page = 1){
		$this->validation($request->all(), [
			"id"=> "required",
			"user_id" => "required",
			"cat_id" => "required",
			"name" => "required",
			"subtitle" => "required",
			"description" => "required",
		]);
		$userinfo =  User::where('id',$request->input('user_id'))->first();
		if (empty($userinfo)) {
			$this->error('User does not exist.', []);
		}
		$user_id=$request->user_id;
		$input['cat_id']=$request->cat_id;
		$input['name']=$request->name;
		$input['subtitle']=$request->subtitle;
		$input['description']=$request->description;
		if($request->file('image')){
			$img_name = $this->uploadS3Data(@$request->file('image'), 'albums');
			$input['image']=$img_name;
		}


		DB::table('contents')->where('id',$request->id)->update($input);

		$skip = 0;
		$skip = $take * ($page - 1);

		$file_path = $this->getPath($request->cat_id);
		$results = Content::where(['cat_id' =>$request->cat_id])->where('id',$request->id)->orderBy('id', 'DESC')->skip($skip)->take($take)->get();
		$response = [];
		foreach ($results as $key => $value) {
			$response[] = [
				'id' => $value->id,
				'category_id' => $value->cat_id,
				'name' => (string) $value->name,
				'subtitle' => (string) $value->subtitle,
				'description' => (string) $value->description,
				'file_image' => $this->getFilePath($value->image, $file_path),
				'is_favourite' => $this->isFavourite($user_id, $value->id, $category_id),
				'created_at' => date('Y-m-d H:i:s', strtotime($value->created_at)),
			];
		}
		return $response;

	}


	/**
     * Get Home all data
     *
     * @param  user_id
     * @return response
     */
	public function getHomeDataNew(Request $request)
	{
		$this->validation($request->all(), [
			"user_id" => "required",
		]);

		$userinfo =  User::where('id',$request->input('user_id'))->first();
		if (empty($userinfo)) {
			$this->error('User does not exist.', []);
		}

		//1=> artist, 2=> album, 3=> playlist, 4=> track
		$banner_data = $this->getBannerData($request->input('user_id'), 3, 5);

		$song_data = $this->getSongData($request->input('user_id'), 2, 10);

		$albums_data = $this->getContentsData($request->input('user_id'), 2, 5);

		$popular_playlist_data = $this->getPopularPlaylistBySong($request->input('user_id'), 3, 5);

		$featured_playlist_data = $this->getFeaturedPlaylistData($request->input('user_id'), 3, 5);

		$response = [];
		$response['banner_data'] = $banner_data;
		$response['song_data'] = $song_data;
		$response['album_data'] = $albums_data;
		$response['popular_playlist_data'] = $popular_playlist_data;
		$response['featured_playlist_data'] = $featured_playlist_data;

		if($response)
		{
			$this->success('record found.', $response);
		}else{
			$this->error('record not fount', []);
		}
	}



	/**
     * get popular playlist by song stats
     *
     * @param  user_id, category_id, take
     * @param  Reference of category: 1=> artist, 2=> album, 3=> playlist, 4=> songs
     * @return response
     */
	public function getPopularPlaylistBySong($user_id = '', $category_id = 3, $take = 5) {

		// $get_data = Stats::where(['user_id'=> $request->input('user_id'), 'song_id'=> $request->input('song_id')])->get();

		$set_temp = DB::select("CREATE TEMPORARY TABLE temp_table as (select sum(stats.play_count) as play_count, songs.id as song_id, contents.id as content_id, contents.cat_id, contents.name, contents.subtitle, contents.description, contents.created_at, contents.image from stats left join songs on stats.song_id=songs.id left join contents on  find_in_set(contents.id, songs.content_id) where contents.cat_id=3  group by  stats.song_id order by stats.play_count DESC);");

		$get_data = DB::select("select * from temp_table group by content_id order by play_count DESC");
		$response = [];
		if (count($get_data)) {
			foreach ($get_data as $key => $value) {
				$file_path = $this->getPath($value->cat_id);
				$response[] = [
					'id' => $value->content_id,
					'category_id' => $value->cat_id,
					'name' => (string) $value->name,
					'subtitle' => (string) $value->subtitle,
					'description' => (string) $value->description,
					'file_image' => $this->getFilePath($value->image, $file_path),
					'is_favourite' => $this->isFavourite($user_id, $value->content_id, $value->cat_id),
					'created_at' => date('Y-m-d H:i:s', strtotime($value->created_at)),
				];
			}
		}else{

			$file_path = $this->getPath($category_id);
			$results = Content::where(['cat_id' => $category_id])->orderBy('id', 'DESC')->take($take)->get();
			$response = [];
			foreach ($results as $key => $value) {
				$response[] = [
					'id' => $value->id,
					'category_id' => $value->cat_id,
					'name' => (string) $value->name,
					'subtitle' => (string) $value->subtitle,
					'description' => (string) $value->description,
					'file_image' => $this->getFilePath($value->image, $file_path),
					'is_favourite' => $this->isFavourite($user_id, $value->id, $category_id),
					'created_at' => date('Y-m-d H:i:s', strtotime($value->created_at)),
				];
			}
		}

		return $response;
	}

	/**
     * get popular playlist by song stats
     *
     * @param  user_id, category_id, take
     * @param  Reference of category: 1=> artist, 2=> album, 3=> playlist, 4=> songs
     * @return response
     */
	public function getPopularSongByStats($user_id = '', $category_id = 3, $take = 5) {

		$get_data = Stats::selectRaw('sum(stats.play_count) as play_count, song_id')->with('getSongInfo')->groupBy('song_id')->orderBy('play_count', 'DESC')->take($take)->get();
		$response = [];
		if (count($get_data)) {
			foreach ($get_data as $key => $value) {
				$file_path = $this->getPath(4);
				$response[] = [
					'song_id' => $value->song_id,
					'name' => (string) $value['getSongInfo']->name,
					'subtitle' => (string) $value['getSongInfo']->subtitle,
					'description' => (string) $value['getSongInfo']->description,
					'song_image' => $this->getFilePath($value['getSongInfo']->image, 'songPic'),
					'song_url' => $this->getFilePath($value['getSongInfo']->song, 'songs'),
					'duration' => $value['getSongInfo']->duration,
					'is_favourite' => $this->isFavourite($user_id, $value->song_id, 4),
					'created_at' => date('Y-m-d H:i:s', strtotime($value['getSongInfo']->created_at)),
					'contents' => $this->getContentsDetails($user_id, $value['getSongInfo']->content_id),
				];
			}
		}else{

			$results = Song::where(['status' => 1])->orderBy('id', 'DESC')->take($take)->get();
			$response = [];
			foreach ($results as $key => $value) {
				$response[] = [
					'song_id' => $value->id,
					'name' => $value->name,
					'subtitle' => $value->subtitle,
					'description' => $value->description,
					'song_image' => $this->getFilePath($value->image, 'songPic'),
					'song_url' => $this->getFilePath($value->song, 'songs'),
					'duration' => $value->duration,
					'is_favourite' => $this->isFavourite($user_id, $value->id, 4),
					'created_at' => date('Y-m-d H:i:s', strtotime($value->created_at)),
					'contents' => $this->getContentsDetails($user_id, $value->content_id),
				];
			}
		}

		return $response;
	}

	/**
     * view popular song by stats
     *
     * @param  user_id, page
     * @param  Reference of category: 1=> artist, 2=> album, 3=> playlist, 4=> songs
     * @return response
     */
	public function viewAllPopularSongs(Request $request) {

		$this->validation($request->all(), [
			"user_id" => "required",
		]);

		$userinfo =  User::where('id',$request->input('user_id'))->first();
		if (empty($userinfo)) {
			$this->error('User does not exist.', []);
		}

		$per_page = 10;
		$take = 10;

		if ($request->input('page') == "") {
			$skip = 0;
		} else {
			$skip = $per_page * ($request->input('page') - 1);
		}

		$get_data = Stats::selectRaw('sum(stats.play_count) as play_count, song_id')->with('getSongInfo')->groupBy('song_id')->orderBy('play_count', 'DESC')->skip($skip)->take($take)->get();
		$response = [];
		if (count($get_data)) {
			foreach ($get_data as $key => $value) {
				$file_path = $this->getPath(4);
				$response[] = [
					'song_id' => $value->song_id,
					'name' => (string) $value['getSongInfo']->name,
					'subtitle' => (string) $value['getSongInfo']->subtitle,
					'description' => (string) $value['getSongInfo']->description,
					'song_image' => $this->getFilePath($value['getSongInfo']->image, 'songPic'),
					'song_url' => $this->getFilePath($value['getSongInfo']->song, 'songs'),
					'duration' => $value['getSongInfo']->duration,
					'is_favourite' => $this->isFavourite($request->input('user_id'), $value->song_id, 4),
					'created_at' => date('Y-m-d H:i:s', strtotime($value['getSongInfo']->created_at)),
					'contents' => $this->getContentsDetails($request->input('user_id'), $value['getSongInfo']->content_id),
				];
			}
		}

		// else{

		// 	$results = Song::where(['status' => 1])->orderBy('id', 'DESC')->take(5)->get();
		// 	$response = [];
		// 	foreach ($results as $key => $value) {
		// 		$response[] = [
		// 			'song_id' => $value->id,
		// 			'name' => $value->name,
		// 			'subtitle' => $value->subtitle,
		// 			'description' => $value->description,
		// 			'song_image' => $this->getFilePath($value->image, 'songPic'),
		// 			'song_url' => $this->getFilePath($value->song, 'songs'),
		// 			'duration' => $value->duration,
		// 			'is_favourite' => $this->isFavourite($request->input('user_id'), $value->id, 4),
		// 			'created_at' => date('Y-m-d H:i:s', strtotime($value->created_at)),
		// 			'contents' => $this->getContentsDetails($request->input('user_id'), $value->content_id),
		// 		];
		// 	}
		// }

		if ($response) {

			$this->success('record found.', $response);
		}else{

			$this->success('record not found.', []);
		}
	}

	/**
     * view popular song by stats
     *
     * @param  user_id, page
     * @param  Reference of category: 1=> artist, 2=> album, 3=> playlist, 4=> songs
     * @return response
     */
	public function viewAllPopularPlaylist(Request $request) {

		$this->validation($request->all(), [
			"user_id" => "required",
		]);

		$userinfo =  User::where('id',$request->input('user_id'))->first();
		if (empty($userinfo)) {
			$this->error('User does not exist.', []);
		}

		$per_page = 10;
		$take = 10;

		if ($request->input('page') == "") {
			$skip = 0;
		} else {
			$skip = $per_page * ($request->input('page') - 1);
		}

		$set_temp = DB::select("CREATE TEMPORARY TABLE temp_table as (select sum(stats.play_count) as play_count, songs.id as song_id, contents.id as content_id, contents.cat_id, contents.name, contents.subtitle, contents.description, contents.created_at, contents.image from stats left join songs on stats.song_id=songs.id left join contents on  find_in_set(contents.id, songs.content_id) where contents.cat_id=3  group by  stats.song_id order by stats.play_count DESC);");

		$get_data = DB::select("select * from temp_table group by content_id order by play_count DESC LIMIT ".@$take." OFFSET ".@$skip." ");
		$response = [];
		if (count($get_data)) {
			foreach ($get_data as $key => $value) {
				$file_path = $this->getPath($value->cat_id);
				$response[] = [
					'id' => $value->content_id,
					'category_id' => $value->cat_id,
					'name' => (string) $value->name,
					'subtitle' => (string) $value->subtitle,
					'description' => (string) $value->description,
					'file_image' => $this->getFilePath($value->image, $file_path),
					'is_favourite' => $this->isFavourite($request->input('user_id'), $value->content_id, $value->cat_id),
					'created_at' => date('Y-m-d H:i:s', strtotime($value->created_at)),
				];
			}
		}else{

			$file_path = $this->getPath(3);
			$results = Content::where(['cat_id' => 3])->orderBy('id', 'DESC')->skip($skip)->take($take)->get();
			$response = [];
			foreach ($results as $key => $value) {
				$response[] = [
					'id' => $value->id,
					'category_id' => $value->cat_id,
					'name' => (string) $value->name,
					'subtitle' => (string) $value->subtitle,
					'description' => (string) $value->description,
					'file_image' => $this->getFilePath($value->image, $file_path),
					'is_favourite' => $this->isFavourite($request->input('user_id'), $value->id, 3),
					'created_at' => date('Y-m-d H:i:s', strtotime($value->created_at)),
				];
			}
		}

		if ($response) {

			$this->success('record found.', $response);
		}else{

			$this->success('record not found.', []);
		}
	}

	/**
     * get contents data acording to category
     *
     * @param  user_id, category_id, take
     * @param  Reference of category: 1=> artist, 2=> album, 3=> playlist, 4=> songs
     * @return response
     */
	public function getBannerData($user_id = '', $category_id = 1, $take = 5) {

		$file_path = $this->getPath($category_id);
		$results = Banner::where(['status'=> 1])->with('getPlaylistInfo')->orderBy('id', 'DESC')->take($take)->get();
		$response = [];
		foreach ($results as $key => $value) {
			$response[] = [
				'id' => $value->id,
				'content_id' => $value->content_id,
				'name' => $value['getPlaylistInfo']->name,
				'subtitle' => $value['getPlaylistInfo']->subtitle,
				'description' => $value['getPlaylistInfo']->description,
				'file_image' => $this->getFilePath($value['getPlaylistInfo']->image, $file_path),
				'is_favourite' => 0,
				'created_at' => date('Y-m-d H:i:s', strtotime($value['getPlaylistInfo']->created_at)),
			];
		}
		return $response;
	}

	/**
     * get contents data acording to category
     *
     * @param  user_id, category_id, take
     * @param  Reference of category: 1=> artist, 2=> album, 3=> playlist, 4=> songs
     * @return response
     */
	public function getFeaturedPlaylistData($user_id = '', $category_id = 1, $take = 5) {

		$file_path = $this->getPath($category_id);
		$results = FeaturedPlaylist::where(['status'=> 1])->with('getPlaylistInfo')->orderBy('id', 'DESC')->take($take)->get();
		$response = [];
		foreach ($results as $key => $value) {
			$response[] = [
				'id' => $value->id,
				'content_id' => $value->content_id,
				'name' => $value['getPlaylistInfo']->name,
				'subtitle' => $value['getPlaylistInfo']->subtitle,
				'description' => $value['getPlaylistInfo']->description,
				'file_image' => $this->getFilePath($value['getPlaylistInfo']->image, $file_path),
				'is_favourite' => 0,
				'created_at' => date('Y-m-d H:i:s', strtotime($value['getPlaylistInfo']->created_at)),
			];
		}
		return $response;
	}

	public function getPath($id='') {
		if ($id == 1) {

			$file_path = 'artists';
		}elseif ($id == 2) {

			$file_path = 'albums';
		}elseif ($id == 3) {

			$file_path = 'playlists';
		}elseif ($id == 4) {

			$file_path = 'tracks';
		}
		elseif ($id == 5) {

			$file_path = 'mySongs';
		}
		return $file_path;
	}

	/**
     * get contents data acording to category
     *
     * @param  user_id, category_id, take
     * @param  Reference of category: 1=> artist, 2=> album, 3=> playlist, 4=> track
     * @return response
     */
	public function getSongData($user_id = '', $category_id = 1, $take = 5) {

		$results = Song::where(['status' => 1])->orderBy('id', 'DESC')->take($take)->get();
		$response = [];
		foreach ($results as $key => $value) {
			$response[] = [
				'song_id' => $value->id,
				'name' => $value->name,
				'subtitle' => $value->subtitle,
				'description' => $value->description,
				'song_image' => $this->getFilePath($value->image, 'songPic'),
				'song_url' => $this->getFilePath($value->song, 'songs'),
				'duration' => $value->duration,
				'is_favourite' => $this->isFavourite($user_id, $value->id, 4),
				'created_at' => date('Y-m-d H:i:s', strtotime($value->created_at)),
				'contents' => $this->getContentsDetails($user_id, $value->content_id),
			];
		}
		return $response;
	}

	/**
     * get contents details acording to contents it
     *
     * @param  user_id, category_id, take
     * @param  Reference of category: 1=> artist, 2=> album, 3=> playlist, 4=> track
     * @return response
     */
	public function getContentsDetails($user_id = '', $contents_ids = 1, $take = 5) {

		if (!empty($contents_ids)) {
			$content_ids = explode(',', $contents_ids);
		}
		$results = Content::whereIn('id', $content_ids)->orderBy('id', 'DESC')->get();
		$response = [];
		foreach ($results as $key => $value) {
			if ($value->cat_id == 1) {
				$response['artist'][] = [
					'id' => $value->id,
					'name' => (string) @$value->name,
					'subtitle' => (string) @$value->subtitle,
					'description' => (string) @$value->description,
					'file_image' => $this->getFilePath($value->image, 'artists'),
					'is_favourite' => $this->isFavourite($user_id, $value->id, 1),
					'created_at' => date('Y-m-d H:i:s', strtotime($value->created_at)),
				];
			}elseif ($value->cat_id == 2) {
				$response['album'][] = [
					'id' => $value->id,
					'name' => (string) @$value->name,
					'subtitle' => (string) @$value->subtitle,
					'description' => (string) @$value->description,
					'file_image' => $this->getFilePath($value->image, 'albums'),
					'is_favourite' => $this->isFavourite($user_id, $value->id, 2),
					'created_at' => date('Y-m-d H:i:s', strtotime($value->created_at)),
				];
			}elseif ($value->cat_id == 3) {
				$response['playlist'][] = [
					'id' => $value->id,
					'name' => (string) @$value->name,
					'subtitle' => (string) @$value->subtitle,
					'description' => (string) @$value->description,
					'file_image' => $this->getFilePath($value->image, 'playlists'),
					'is_favourite' => $this->isFavourite($user_id, $value->id, 3),
					'created_at' => date('Y-m-d H:i:s', strtotime($value->created_at)),
				];
			}elseif ($value->cat_id == 4) {
				$response['genre'][] = [
					'id' => $value->id,
					'name' => (string) @$value->name,
					'subtitle' => (string) @$value->subtitle,
					'description' => (string) @$value->description,
					'file_image' => $this->getFilePath($value->image, 'tracks'),
					'is_favourite' => 0, //it will not favourite
					'created_at' => date('Y-m-d H:i:s', strtotime($value->created_at)),
				];
			}

		}
		return $response;
	}



	/**
     * Get All Songs
     *
     * @param  user_id
     * @return response
     */
	public function getAllSongs(Request $request) {

		$this->validation($request->all(), [
			"user_id" => "required",
		]);

		$per_page = 10;
		$take = 10;

		if ($request->input('page') == "") {
			$skip = 0;
		} else {
			$skip = $per_page * ($request->input('page') - 1);
		}

		$take = 20;
		$results = Song::where(['status' => 1])->orderBy('id', 'DESC')->skip($skip)->take($take)->get();
		$response = [];
		foreach ($results as $key => $value) {
			$response[] = [
				'song_id' => $value->id,
				'name' => $value->name,
				'subtitle' => $value->subtitle,
				'description' => $value->description,
				'song_image' => $this->getFilePath($value->image, 'songPic'),
				'song_url' => $this->getFilePath($value->song, 'songs'),
				'duration' => $value->duration,
				'is_favourite' => $this->isFavourite($request->input('user_id'), $value->id, 4),
				'created_at' => date('Y-m-d H:i:s', strtotime($value->created_at)),
				'contents' => $this->getContentsDetails($request->input('user_id'), $value->content_id),
			];
		}
		if ($response) {
			$this->success('record found.', $response);
		}else{
			$this->error('record not found.', []);
		}
	}

	/**
     * Get All Tracks
     *
     * @param  user_id
     * @return response
     */
	public function getSongsByGroup(Request $request) {

		$this->validation($request->all(), [
			"user_id" => "required",
			"content_id" => "required",
		]);

		$take = 10;

		if ($request->input('page') == "") {
			$skip = 0;
		} else {
			$skip = $take * ($request->input('page') - 1);
		}

		$results = Song::whereRaw('FIND_IN_SET("'.$request->input('content_id').'", content_id)')->orderBy('id', 'DESC')->skip($skip)->take($take)->get();
		$response = [];
		foreach ($results as $key => $value) {
			$response[] = [
				'song_id' => $value->id,
				'name' => $value->name,
				'subtitle' => $value->subtitle,
				'description' => $value->description,
				'song_image' => $this->getFilePath($value->image, 'songPic'),
				'song_url' => $this->getFilePath($value->song, 'songs'),
				'duration' => $value->duration,
				'is_favourite' => $this->isFavourite($request->input('user_id'), $value->id, 4),
				'created_at' => date('Y-m-d H:i:s', strtotime($value->created_at)),
				'contents' => $this->getContentsDetails($request->input('user_id'), $value->content_id),
			];
		}
		if ($response) {
			$this->success('record found.', $response);
		}else{
			$this->error('record not found.', []);
		}
	}

	/**
     * get contents info
     *
     * @param  user_id, category_id, take
     * @param  Reference of category: 1=> artist, 2=> album, 3=> playlist, 4=> genre
     * @return response
     */
	public function getContentsInfo(Request $request) {
		$this->validation($request->all(), [
			"user_id" => "required",
			"type_id" => "required",
		]);

		$take = 10;

		if ($request->input('page') == "") {
			$page = 0;
		} else {
			$page = $take * ($request->input('page') - 1);
		}

		$response = [];
		$response = $this->getContentsData($request->input('user_id'), $request->input('type_id'), $take, $page);
		if ($response) {
			$this->success('record found.', $response);
		}else{
			$this->error('record not found.');
		}

	}
/**
*Get Category
*/

public function getCategoryes(Request $request){
	$this->validation($request->all(), [
		"user_id" => "required",
		"type_id" => "required",
	]);
	$file_path = $this->getPath($request->type_id);
	$url = Storage::disk('s3')->url($file_path);
	$response=DB::table('contents')->where('cat_id',$request->type_id)->where('status',1)->get();
	if($response->isEmpty()){
		$this->error('record not found.');
	}else{
		foreach ($response as $key => $value) {
			$value->image=$url.'/'.$value->image;
		}
		$this->success('record found.', $response);
	}
}


public function randomCategory(Request $request){
	$this->validation($request->all(), [
		"user_id" => "required",
		"type_id" => "required",
	]);
	$file_path = $this->getPath($request->type_id);
	$url = Storage::disk('s3')->url($file_path);

	if(Content::where('cat_id',$request->type_id)->whereDate('random_date',date('Y-m-d'))->where('random_day','Monday')->count('id') > 0){
	}else{
		$find=Content::where('cat_id',$request->type_id)->inRandomOrder()->first();
			if(!empty($find)){
				if(date('l')=='Monday'){
					Content::whereNotNull('random_date')->update(array('random_date'=>null));
					Content::where('id',$find->id)->update(array('random_date'=>date('Y-m-d')));
				}
			}	
	}
		
	
	$response['random']=Content::where('cat_id',$request->type_id)->whereNotNull('random_date')->inRandomOrder()->first();
	if(!empty($response['random'])){
		foreach ($response as $key => $ft) {
			$ft->image=$url.'/'.$ft->banner;
		}
	}
	$response['interested']=array();
	$file_path = $this->getPath($request->type_id);
	$interest=DB::table('callect_category')->where('callect_userid',$request->user_id)->get();
	if(!$interest->isEmpty()){
	foreach($interest as $ft){
		$category_id=json_decode($ft->callect_categoryid);
		if(!empty($category_id)){
			$response['interested']=DB::table('contents')->whereIn('id',$category_id)->where('status',1)->get();
			foreach($response['interested'] as $catid){
				$catid->image=$url.'/'.$catid->image;
				$catid->user_id=$ft->callect_userid;
			}
		}
	}
}else{
		$response['interested']=DB::table('contents')->whereIn('id',array(49,58,69))->where('status',1)->get();
			foreach($response['interested'] as $catid){
				$catid->image=$url.'/'.$catid->image;
				$catid->user_id=$request->user_id;
			}
}



	$file_path = $this->getPath($request->type_id);
	$response['categories']=DB::table('contents')->where('cat_id',$request->type_id)->where('status',1)->get();
	if($response['categories']->isEmpty()){
		$this->error('record not found.');
	}else{
		foreach ($response['categories'] as $key => $value) {
			$value->image=$url.'/'.$value->image;
		}
	}

	$array[]=array(
		'id'=>1,
		'type'=>'My Recordings',
		'date'=>date('d-m-Y'),
	);

	$array[]=array(
		'id'=>1,
		'type'=>'My Favourite',
		'date'=>date('d-m-Y'),
	);

	$response['mystuff']=$array;
	/*$response['nature']=DB::table('nature')->where('nature_status',1)->where('music_type','Nature')->get();
	foreach($response['nature'] as $nature){
		$count=DB::table('userPayments')->where('payment_users',$request->user_id)->where('payment_plan_id',$nature->nature_id)->count();
		if($count>0){
			$nature->images=asset('public/images/flag').'/'.$nature->nature_image;

		}else{
			$nature->images=asset('public/images/flag').'/'.$nature->lock_images;
		}
		$nature->songs=asset('public/voice').'/'.$nature->nature_sound;

	}*/

	$response['nature']=DB::table('nature')->where('nature_status',1)->where('music_type','Nature')->get();
	foreach($response['nature'] as $nature){

		$date=User::where('id',$request->user_id)->first();
		$datetime1 = date_create(date('d-m-Y',strtotime($date->updated_at))); 
		$datetime2 = date_create(date('d-m-Y')); 
		$interval = date_diff($datetime1, $datetime2); 
		$day=$interval->format('%a'); 
		//echo $day;
		if($day < 90){
			$nature->images=asset('public/images/flag').'/'.$nature->nature_image;
			$nature->lockUnlockStatus=1;
		}else{

			$subscripion=DB::table('userPayments')->where('payment_users',$request->user_id)->where('subscription','subscription')->orderBy('id','desc')->limit(1)->first();
					//print_r($subscripion);
			if(!empty($subscripion)){
				if($subscripion->package_type==1){
					$subscripionDate=strtotime($subscripion->payment_date.'+ 1 months');
					if($subscripionDate > strtotime(date('Y-m-d'))){
						$nature->images=asset('public/images/flag').'/'.$nature->nature_image;
						$nature->lockUnlockStatus=1;
					}else{
						$nature->images=asset('public/images/flag').'/'.$nature->lock_images;
						$nature->lockUnlockStatus=0;
					}

				}elseif($subscripion->package_type==2){
					$subscripionDate=strtotime($subscripion->payment_date.'+ 12 months');
					if($subscripionDate > strtotime(date('Y-m-d'))){
						$nature->images=asset('public/images/flag').'/'.$nature->nature_image;
						$nature->lockUnlockStatus=1;
					}else{
						$nature->images=asset('public/images/flag').'/'.$nature->lock_images;
						$nature->lockUnlockStatus=0;
					}
				}else{
					$count=DB::table('userPayments')->where('payment_users',$request->user_id)->where('payment_plan_id',$nature->nature_id)->count();
					if($count>0){
						$nature->images=asset('public/images/flag').'/'.$nature->nature_image;
						$nature->lockUnlockStatus=1;
					}else{
						$nature->images=asset('public/images/flag').'/'.$nature->lock_images;
						$nature->lockUnlockStatus=0;
					}
				}
			}else{
				$count=DB::table('userPayments')->where('payment_users',$request->user_id)->where('payment_plan_id',$nature->nature_id)->count();
				if($count>0){
					$nature->images=asset('public/images/flag').'/'.$nature->nature_image;
					$nature->lockUnlockStatus=1;
				}else{
					$nature->images=asset('public/images/flag').'/'.$nature->lock_images;
					$nature->lockUnlockStatus=0;
				}
			}

					// $count=DB::table('userPayments')->where('payment_users',$request->user_id)->where('payment_plan_id',$nature->nature_id)->count();
					// if($count>0){
					// $nature->images=asset('public/images/flag').'/'.$nature->nature_image;
					// $nature->lockUnlockStatus=1;
					// }else{
					// $nature->images=asset('public/images/flag').'/'.$nature->lock_images;
					// $nature->lockUnlockStatus=0;
					// }
		}
		$nature->songs=asset('public/voice').'/'.$nature->nature_sound;

	}




	$this->success('record found.', $response);

}



	/**
     * Add fa
     *
     * @param  user_id
     * @return response
     * @return type 1=> artist, 2=> album, 3=> playlist, 4=> song
     */
	public function addFavourite(Request $request)
	{
		$this->validation($request->all(), [
			"user_id" => "required",
			"type" 	  => "required",
			"fav_id"  => "required",
		]);

		$check_favourite =  Favourite::where(['user_id'=>$request->input('user_id'), 'type'=>$request->input('type'), 'fav_id'=>$request->input('fav_id')])->first();

		if(!empty($check_favourite)){
			if($check_favourite->status == 1){
				$status = 0;
			}else{
				$status = 1;
			}
			$store_data = Favourite::where(['user_id'=>$request->input('user_id'), 'type'=>$request->input('type'), 'fav_id'=>$request->input('fav_id')])->update(['status' => $status]);
		}else{
			$status = 1;
			$data = ['user_id'=>$request->input('user_id'), 'type'=>$request->input('type'), 'fav_id'=>$request->input('fav_id'), 'status' => 1];
			$store_data = Favourite::insert($data);
		}

		if (@$store_data) {

			$response = [];
			$response['is_favourite'] = $status;
			$this->success('success', $response);
		}else{
			$this->error('Something went wrong.');
		}


	}

     /**
     * check Favourite
     *
     * @param  type 1=> artist, 2=> album, 3=> playlist, 4=> song
     * @return response
     */
     public function isFavourite($user_id='', $fav_id = '', $type_id = '')
     {
     	$result =  Favourite::where(['user_id'=>$user_id, 'type'=>$type_id, 'fav_id'=>$fav_id])->first();
     	return (int) @$result['status'];
     }

     /**
     * Get User All Playlists
     *
     * @param  user_id
     * @return response
     */
     public function getPlaylists(Request $request)
     {
     	$this->validation($request->all(), [
     		"user_id" => "required",
     	]);

     	$userinfo =  User::where('id',$request->input('user_id'))->first();

     	if($userinfo){

     		$playlists = UserPlaylist::where('user_id', $request->input('user_id'))->get();

     		if(count($playlists)>0)
     		{
     			$this->success('All Playlist.', $playlists);
     		}else{
     			$this->error('Playlist does not exist.', []);
     		}
     	}else{
     		$this->error('User does not exist.', []);
     	}
     }




	/**
     * Create User Playlist
     *
     * @param  user_id
     * @return response
     */
	public function createPlaylist(Request $request)
	{
		$this->validation($request->all(), [
			"user_id" => "required",
			"name" => "required"
		]);

		$userinfo =  User::where('id',$request->input('user_id'))->first();

		if($userinfo){

			$data = [
				'user_id'=> $request->input('user_id'),
				'name'=> strtolower($request->input('name')),
				'description'=> strtolower($request->input('description')),
			];

			if ($save = UserPlaylist::create($data)) {

				$response['playlist_id'] = (string) $save->id;
				$this->success('Playlist created successfully.', $response);
			}else{
				$this->error('Something went wrong.', null);
			}
		}else{
			$this->error('User does not exist.', null);
		}
	}


	//get user profile using from user_id
	public function getUserProfile($user_id='') {
		$userinfo =  User::where('id', $user_id)->first();
		$picture = "";
		if ($userinfo) {
			if(strstr($userinfo->profile, 'https://')){
				$picture = $userinfo->profile;
			}elseif(!empty($userinfo->profile)){
				$picture = Controller::getFilePath($userinfo->profile, 'users');
				// $picture = URL::to('/uploads/users/').'/'.$userinfo->profile;
			}
		}
		return $picture;
	}


    /**
     * Get All Tracks of an Artist
     *
     * @param  user_id
     * @return response
     */
    public function getTracksByArtist(Request $request)
    {
    	$artist =  Content::where('id',$request->input('artist_id'))->first();

    	if($artist)
    	{
    		$tracks = Song::where(['cat_id'=>'1', 'content_id'=>$request->input('artist_id'), 'status'=>'1'])->orderBy('id', 'DESC')->get();

    		if(count($tracks)>0)
    		{
    			foreach ($tracks as $key => $value) {

    				$artist_id=$album_id=$playlist_id=$artist=$album=$playlist="";

    				$artist_id = Song::where(['group_id'=>$value->group_id, 'cat_id'=>'1'])->pluck('content_id')->first();

    				$album_id = Song::where(['group_id'=>$value->group_id, 'cat_id'=>'2'])->pluck('content_id')->first();

    				$playlist_id = Song::where(['group_id'=>$value->group_id, 'cat_id'=>'3'])->pluck('content_id')->first();

    				if(!empty($artist_id))
    				{
    					$artist = Content::where('id',$artist_id)->pluck('name')->first();
    				}

    				if(!empty($album_id))
    				{
    					$album = Content::where('id',$album_id)->pluck('name')->first();
    				}
    				if(!empty($playlist_id))
    				{
    					$playlist = Content::where('id',$playlist_id)->pluck('name')->first();
    				}

    				if(!empty($value->content_id))
    				{
    					$track = Content::where('id',$value->content_id)->pluck('name')->first();
    				}

    				$data[] = [
    					"id" 	  	  => (string) $value->id,
    					"title" 	  => (string) $value->name,
    					"subtitle"	  => (string) $value->subtitle,
    					"description" => (string) $value->description,
    					"image"	 	  => (string) !empty($value->image)?Controller::getFilePath($value->image, 'songPic'):"",
    					"song"	 	  => (string) !empty($value->song)?Controller::getFilePath($value->song, 'songs'):"",
    					"duration"	  => (string) $value->duration,
    					"artist"	  => (string) $artist,
    					"album"	  	  => (string) $album,
    					"playlist"	  => (string) $playlist,
    					"track"	  	  => (string) $track,
    				];
    			}
    			$this->success('All tracks.', $data);
    		}else{
    			$this->success('All tracks.', []);
    		}
    	}else{
    		$this->error('Artist does not exist.', null);
    	}
    }


	/**
     * Get All Tracks of an Artist
     *
     * @param  user_id
     * @return response
     */
	public function getTracksByAlbum(Request $request)
	{
		$album =  Content::where('id',$request->input('album_id'))->first();

		if($album)
		{
			$tracks = Song::where(['cat_id'=>'2', 'content_id'=>$request->input('album_id'), 'status'=>'1'])->orderBy('id', 'DESC')->get();

			if(count($tracks)>0)
			{
				foreach ($tracks as $key => $value) {

					$artist_id=$album_id=$playlist_id=$artist=$album=$playlist="";

					$artist_id = Song::where(['group_id'=>$value->group_id, 'cat_id'=>'1'])->pluck('content_id')->first();

					$album_id = Song::where(['group_id'=>$value->group_id, 'cat_id'=>'2'])->pluck('content_id')->first();

					$playlist_id = Song::where(['group_id'=>$value->group_id, 'cat_id'=>'3'])->pluck('content_id')->first();

					if(!empty($artist_id))
					{
						$artist = Content::where('id',$artist_id)->pluck('name')->first();
					}

					if(!empty($album_id))
					{
						$album = Content::where('id',$album_id)->pluck('name')->first();
					}
					if(!empty($playlist_id))
					{
						$playlist = Content::where('id',$playlist_id)->pluck('name')->first();
					}

					if(!empty($value->content_id))
					{
						$track = Content::where('id',$value->content_id)->pluck('name')->first();
					}

					$data[] = [
						"id" 	  	  => (string) $value->id,
						"title" 	  => (string) $value->name,
						"subtitle"	  => (string) $value->subtitle,
						"description" => (string) $value->description,
						"image"	 	  => (string) !empty($value->image)?Controller::getFilePath($value->image, 'songPic'):"",
						"song"	 	  => (string) !empty($value->song)?Controller::getFilePath($value->song, 'songs'):"",
						"duration"	  => (string) $value->duration,
						"artist"	  => (string) $artist,
						"album"	  	  => (string) $album,
						"playlist"	  => (string) $playlist,
						"track"	  	  => (string) $track,
					];
				}
				$this->success('All tracks.', $data);
			}else{
				$this->success('All tracks.', []);
			}
		}else{
			$this->error('Album does not exist.', null);
		}
	}


	public function getVoice(Request $request){
		$voice=DB::table('voice')->where('status',1)->get();
		if(!$voice->isEmpty()){
			foreach($voice as $ft){
				$ft->image=asset('public').'/images/'.$ft->image;
				$ft->flag=asset('public').'/images/flag/'.$ft->flag;
				$ft->voices=asset('public').'/voice/'.$ft->voices;
			}
			$data=$voice;

			$this->success('record found.',$data);
		}else{
			$data=array();
			$this->error('record not found.',$data);
		}
	}


	public function setVoice(Request $request){
		$this->validation($request->all(), [
			"user_id" => "required",
			"voice_id" => "required",
			"user_time" => "required",
			"status" => "required",
		]);

		$input['voice_id']=json_encode($request->voice_id);
		$input['user_id']=$request->user_id;
		$input['user_time']=$request->user_time;
		$input['on_off_status']=$request->status;



		$data=DB::table('user_voice')->insert($input);
		$id= DB::getPdo()->lastInsertId();
		$record=DB::table('user_voice')->where('user_voice_id',$id)->where('user_id',$request->user_id)->get();
		$this->success("record found",$record);
	}

	public function collectCategory(Request $request){

		$this->validation($request->all(), [
			"user_id" => "required",
			"category_id" => "required",
		]);
		$input['callect_categoryid']=json_encode($request->category_id);
		$input['callect_userid']=$request->user_id;
		DB::table('callect_category')->insert($input);
		$this->success("Successfully insert category");
	}


	public function support(Request $request){
		$array=array(
			'support_userid'=>$request->user_id,
			'support_title'=>$request->suppert_subject,
			'support_message'=>$request->support_message,
			'type'=>'1'
		);
		$records=DB::table('supports')->insert($array);
		$userinfo=user::where('id',$request->user_id)->first();
        // $data=['email' => $userinfo->email, 'username' => $userinfo->first_name, 'title'=>$request->suppert_subject,'message'=>$request->support_message];

		$send = Mail::send('emails.Support', ['email' => $userinfo->email, 'username' => $userinfo->first_name, 'title'=>$request->suppert_subject,'user_message'=>$request->support_message], function ($m) use ($userinfo) {
			$m->from('no-reply@imarkinfotech.com', 'meditation');
			$m->to($userinfo->email, $userinfo->first_name)->subject('meditation : Support');
		});


		$this->success("Successfully send you query");
	}


	public function help(Request $request){
		$this->validation($request->all(), [
			"user_id" => "required",
		]);
		$record=DB::table('usershelps')->where('help_status',1)->get();
		if(!$record->isEmpty()){
			$this->success("Records found !",$record);
		}else{
			$this->error("Records not found !");
		}
	}

	public function mySongsList(Request $request){
		$this->validation($request->all(), [
			"user_id" => "required",
			"cat_id"=>"required",
		]);

	 /*$file_path = $this->getPath(3);
	 $url = Storage::disk('s3')->url($file_path);*/
	 $file_path1 = $this->getPath(5);
	 $url1 = Storage::disk('s3')->url($file_path1);

	 $file_path = $this->getPath(2);
	 $url = Storage::disk('s3')->url($file_path);
     // $record=DB::table('mysongs')->where('songs_status',1)->where('user_id',$request->user_id)->get();
	 $record=DB::table('contents')->select('contents.banner','contents.id','contents.created_at','contents.description','contents.image','contents.subtitle','mysongs.*','contents.backgoround_images')
	 ->join('mysongs','mysongs.cat_id','contents.id')->where('mysongs.user_id',$request->user_id)->where('contents.id',$request->cat_id)->get();
	 foreach($record as $row){
	 	$row->image=$url.'/'.$row->backgoround_images;
	 	if($row->songs==''){
	 		$row->upload_status=0;
	 	}else{
	 		$row->upload_status=1;
	 	}
	 	$row->songs=$url1.'/'.$row->songs;
	 }
	 if(!$record->isEmpty()){
	 	$this->success("Records found !",$record);
	 }else{
	 	$this->error("Records not found !");
	 }
	}


	public function myRecording(Request $request){
		$this->validation($request->all(), [
			"user_id" => "required",
		]);
		$file_path = $this->getPath(2);
		$url = Storage::disk('s3')->url($file_path);
		$file_path1 = $this->getPath(5);
		$url1 = Storage::disk('s3')->url($file_path1);


     	// $response['playAll']=DB::table('mysongs')->where('user_id',$request->user_id)->get();
     	// foreach($response['playAll'] as $ft){
     	// 	$ft->songs=$url1.'/'.$ft->songs;
     	// }
		$playall=array();
		$records['playAll']=DB::table('mysongs')->where('user_id',$request->user_id)->get();
		foreach($records['playAll'] as $ft){
			if($ft->songs!=''){
				$songs=$url1.'/'.$ft->songs;
				array_push($playall, $songs);

			}
		}
		$response['playall']=$playall;
		$catArray=array();
		$category_id=DB::table('mysongs')->where('user_id',$request->user_id)->groupBy('cat_id')->get();

		foreach($category_id as $ft){
			$catId=$ft->cat_id;
			array_push($catArray,$catId);
		}

		//$response['category']=array();
		//if(!empty($catArray)){
			//$response['category']=Content::whereIn('id',$catArray)->get();
			$response['category']=Content::where('cat_id',2)->get();
			foreach($response['category'] as  $key => $row){
				$row->image=$url.'/'.$row->image;
				$counts=DB::table('mysongs')->where('user_id',$request->user_id)->where('cat_id',$row->id)->count();
				$row->count=$counts;
				$response['category'][$key]['myRecording']=DB::table('mysongs')->select('songs')->where('user_id',$request->user_id)->where('cat_id',$row->id)->get();
				$response1=array();
				foreach($response['category'][$key]['myRecording'] as $rows){
     			//$rows->songs=$url1.'/'.$rows->songs;
					$songs=$url1.'/'.$rows->songs;
					array_push($response1,$songs);
				}
				$response['category'][$key]['myRecording']= $response1;
			}

		//}

		$this->success('My recording songs',$response);






	}

	
	
	public function music(Request $request){
		$this->validation($request->all(), [
			"user_id" => "required",
		]);
		$response['SoundScopes']=DB::table('nature')->where('nature_status',1)->where('music_type','Nature')->get();
		foreach($response['SoundScopes'] as $nature){


			$date=User::where('id',$request->user_id)->first();
			$datetime1 = date_create(date('d-m-Y',strtotime($date->updated_at))); 
			$datetime2 = date_create(date('d-m-Y')); 
			$interval = date_diff($datetime1, $datetime2); 
			$day=$interval->format('%a'); 
		//echo $day;
			if($day < 90){
				$nature->images=asset('public/images/flag').'/'.$nature->nature_image;
				$nature->lockUnlockStatus=1;
			}else{

				$subscripion=DB::table('userPayments')->where('payment_users',$request->user_id)->where('subscription','subscription')->orderBy('id','desc')->limit(1)->first();
					//print_r($subscripion);
				if(!empty($subscripion)){
					if($subscripion->package_type==1){
						$subscripionDate=strtotime($subscripion->payment_date.'+ 1 months');
						if($subscripionDate > strtotime(date('Y-m-d'))){
							$nature->images=asset('public/images/flag').'/'.$nature->nature_image;
							$nature->lockUnlockStatus=1;
						}else{
							$nature->images=asset('public/images/flag').'/'.$nature->lock_images;
							$nature->lockUnlockStatus=0;
						}

					}elseif($subscripion->package_type==2){
						$subscripionDate=strtotime($subscripion->payment_date.'+ 12 months');
						if($subscripionDate > strtotime(date('Y-m-d'))){
							$nature->images=asset('public/images/flag').'/'.$nature->nature_image;
							$nature->lockUnlockStatus=1;
						}else{
							$nature->images=asset('public/images/flag').'/'.$nature->lock_images;
							$nature->lockUnlockStatus=0;
						}
					}else{
						$count=DB::table('userPayments')->where('payment_users',$request->user_id)->where('payment_plan_id',$nature->nature_id)->count();
						if($count>0){
							$nature->images=asset('public/images/flag').'/'.$nature->nature_image;
							$nature->lockUnlockStatus=1;
						}else{
							$nature->images=asset('public/images/flag').'/'.$nature->lock_images;
							$nature->lockUnlockStatus=0;
						}
					}
				}else{
					$count=DB::table('userPayments')->where('payment_users',$request->user_id)->where('payment_plan_id',$nature->nature_id)->count();
					if($count>0){
						$nature->images=asset('public/images/flag').'/'.$nature->nature_image;
						$nature->lockUnlockStatus=1;
					}else{
						$nature->images=asset('public/images/flag').'/'.$nature->lock_images;
						$nature->lockUnlockStatus=0;
					}
				}

			}


			$nature->songs=asset('public/voice').'/'.$nature->nature_sound;

		}

		$response['Music']=DB::table('nature')->where('nature_status',1)->where('music_type','Music')->get();
		foreach($response['Music'] as $Music){

			$date=User::where('id',$request->user_id)->first();
			$datetime1 = date_create(date('d-m-Y',strtotime($date->updated_at))); 
			$datetime2 = date_create(date('d-m-Y')); 
			$interval = date_diff($datetime1, $datetime2); 
			$day=$interval->format('%a'); 
			if($Music->price==0){
				$Music->images=asset('public/images/flag').'/'.$Music->nature_image;
				$Music->lockUnlockStatus=0;
			}elseif($day < 90){
				$Music->images=asset('public/images/flag').'/'.$Music->nature_image;
				$Music->lockUnlockStatus=1;
			}else{

				$subscripion=DB::table('userPayments')->where('payment_users',$request->user_id)->where('subscription','subscription')->orderBy('id','desc')->limit(1)->first();

				if(!empty($subscripion)){
					if($subscripion->package_type==1){
						$subscripionDate=strtotime($subscripion->payment_date.'+ 1 months');
						if($subscripionDate > strtotime(date('Y-m-d'))){
							$Music->images=asset('public/images/flag').'/'.$Music->nature_image;
							$Music->lockUnlockStatus=1;
						}else{
							$Music->images=asset('public/images/flag').'/'.$Music->nature_image;
							$Music->lockUnlockStatus=0;
						}
					}elseif($subscripion->package_type==2){
						$subscripionDate=strtotime($subscripion->payment_date.'+ 12 months');
						if($subscripionDate > strtotime(date('Y-m-d'))){
							$Music->images=asset('public/images/flag').'/'.$Music->nature_image;
							$Music->lockUnlockStatus=1;
						}else{
							$Music->images=asset('public/images/flag').'/'.$Music->nature_image;
							$Music->lockUnlockStatus=0;
							$Music->name=$Music->nature_name;
						}
					}else{
						$count=DB::table('userPayments')->where('payment_users',$request->user_id)->where('payment_plan_id',$Music->nature_id)->count();
						if($count>0){
							$Music->images=asset('public/images/flag').'/'.$Music->nature_image;
							$Music->lockUnlockStatus=1;
						}else{
							$Music->images=asset('public/images/flag').'/'.$Music->nature_image;
							$Music->lockUnlockStatus=0;
							$Music->name=$Music->nature_name;
						}
					}
				}else{
					$count=DB::table('userPayments')->where('payment_users',$request->user_id)->where('payment_plan_id',$Music->nature_id)->count();
					if($count>0){
						$Music->images=asset('public/images/flag').'/'.$Music->nature_image;
						$Music->lockUnlockStatus=1;
					}else{
						$Music->images=asset('public/images/flag').'/'.$Music->nature_image;
						$Music->lockUnlockStatus=0;
						$Music->name=$Music->nature_name;
					}
				}
			}


			$Music->songs=asset('public/voice').'/'.$Music->nature_sound;

		}
		$this->success('record found.', $response);

	}

	
	public function affirmationCategoies(Request $request)
	{
		$this->validation($request->all(), [
			"user_id" => "required",
			"cat_id" => "required",
		]);
		$file_path = $this->getPath(2);
		$url = Storage::disk('s3')->url($file_path);
		$picture1 = Storage::disk('s3')->url('albums');
		$circlar = Storage::disk('s3')->url('circular');
		$response['affirmation']=DB::table('contents')->where('id',$request->cat_id)->get();
		foreach($response['affirmation'] as $row){
			$row->id=$row->id;
			$row->cat_id=$row->cat_id;
			$row->name=$row->name;
			$row->name=$row->subtitle;
			$row->description=$row->description;
			$row->image=$picture1.'/'.$row->backgoround_images;
			$row->status=$row->status;
			$row->circular_images=$circlar.'/'.$row->circular_images;
		}

		// $images="http://meditation.customer-devreview.com/public/images/flag/1589795059.png";
		// $name="Session";
		// $array=array();
		//  for($i=1;$i<=6;$i++){
		//  	$array[]=array(
		//  			'images'=>$images,
		//  			'name'=>$name.$i
		//  	);
		//  }
		//  $response['session']=$array;

		$picture1 = Storage::disk('s3')->url('songPic');
		$songs = Storage::disk('s3')->url('songs');
		$userVoice=DB::table('user_voice')->where('user_id',$request->user_id)->orderBy('user_voice_id','desc')->first();
		 $voice_id=json_decode($userVoice->voice_id);
		 DB::enableQueryLog(); 

		 if(empty($userVoice)){
		 	$voice_id=array(1);
		 }

		$response['session']=DB::table('affirmation')->select('affirmation.id as nature_id','affirmation.affirmation_title as nature_name','affirmation.affirmation_images as nature_image','affirmation.affirmation_images as icon','affirmation.lock_images as lock_images','affirmation.affirmation_songs as nature_sound','affirmation.free_premium as price','affirmation.affirmation_duration as duration','affirmation.songs_type as music_type','affirmation.date as nature_date','affirmation.status as nature_status','affirmation.free_premium as free_premium','affirmation.affirmation_songs','affirmation.affirmation_images','affirmation.affirmation_title','affirmation.free_premium')->where('cat_id',$request->cat_id)
			->where(function($query) use($voice_id){
			if(!empty($voice_id)){
				$query->whereIn('artist_id',$voice_id);
			}
			})
			->where('affirmation.status',1)
			->get();

			//dd(DB::getQueryLog());
		
		foreach($response['session'] as $Music){

				if(DB::table('session_favourites')->where('favourite_session_id',$Music->nature_id)->where('favourite_userid',$request->user_id)->where('favourite_status',1)->count() > 0){
					$Music->favourite=1;
				}else{
					$Music->favourite=0;
				}

			$date=User::where('id',$request->user_id)->first();
			$datetime1 = date_create(date('d-m-Y',strtotime($date->updated_at))); 
			$datetime2 = date_create(date('d-m-Y')); 
			$interval = date_diff($datetime1, $datetime2); 
			$day=$interval->format('%a'); 
			// if($Music->free_premium==0){
			// 	$Music->images=$picture1.'/'.$Music->affirmation_images;
			// 	$Music->lockUnlockStatus=0;
			// 	$Music->name=$Music->affirmation_title;
			// }else

			if($day < 90){
				$Music->images=$picture1.'/'.$Music->affirmation_images;
				$Music->lockUnlockStatus=1;
				$Music->name=$Music->affirmation_title;
			}else{

				$subscripion=DB::table('userPayments')->where('payment_users',$request->user_id)->where('subscription','subscription')->orderBy('id','desc')->limit(1)->first();

				if(!empty($subscripion)){
					if($subscripion->package_type==1){
						$subscripionDate=strtotime($subscripion->payment_date.'+ 1 months');
						if($subscripionDate > strtotime(date('Y-m-d'))){
							$Music->images=$picture1.'/'.$Music->affirmation_images;
							$Music->lockUnlockStatus=1;
							$Music->name=$Music->affirmation_title;
						}else{
							$Music->images=$picture1.'/'.$Music->affirmation_images;
							$Music->lockUnlockStatus=0;
							$Music->name=$Music->affirmation_title;
						}
					}elseif($subscripion->package_type==2){
						$subscripionDate=strtotime($subscripion->payment_date.'+ 12 months');
						if($subscripionDate > strtotime(date('Y-m-d'))){
							$Music->images=$picture1.'/'.$Music->affirmation_images;
							$Music->lockUnlockStatus=1;
							$Music->name=$Music->affirmation_title;
						}else{
							$Music->images=$picture1.'/'.$Music->affirmation_images;
							$Music->lockUnlockStatus=0;
							$Music->name=$Music->affirmation_title;
						}
					}else{
						$count=DB::table('userPayments')->where('payment_users',$request->user_id)->where('payment_plan_id',$Music->id)->count();
						if($count>0){
							$Music->images=$picture1.'/'.$Music->affirmation_images;
							$Music->lockUnlockStatus=1;
							$Music->name=$Music->affirmation_title;;
						}else{
							$Music->images=$picture1.'/'.$Music->affirmation_images;
							$Music->lockUnlockStatus=0;
							$Music->name=$Music->affirmation_title;
						}
					}
				}else{
					$count=DB::table('userPayments')->where('payment_users',$request->user_id)->where('payment_plan_id',$Music->id)->count();
					if($count>0){
						$Music->images=$picture1.'/'.$Music->affirmation_images;
						$Music->lockUnlockStatus=1;
						$Music->name=$Music->affirmation_title;
					}else{
						$Music->images=$picture1.'/'.$Music->affirmation_images;
						$Music->lockUnlockStatus=0;
						$Music->name=$Music->affirmation_title;
					}
				}
			}		

			$Music->songs=$songs.'/'.$Music->affirmation_songs;

		}







			/*$response['session']=DB::table('affirmation')->where('cat_id',$row->id)->get();
			foreach($response['session'] as $ft){
				$ft->affirmation_songs=$ft->affirmation_songs;
				$ft->affirmation_images=$ft->affirmation_images;
				$ft->affirmation_duration=$ft->affirmation_duration;
				$ft->affirmation_title=$ft->affirmation_title;
				$ft->affirmation_discription=$ft->affirmation_discription;
				$ft->affirmation_favrites=$ft->affirmation_favrites;
			}*/


			$file_path = $this->getPath(2);
			$url = Storage::disk('s3')->url($file_path);
			$response['recomended']=Content::where('id','!=',$request->cat_id)->where('cat_id',2)->inRandomOrder()->limit(2)->get();
			if(!empty($response['recomended'])){
				foreach ($response['recomended'] as $key => $ft) {
					$ft->image=$url.'/'.$ft->image;
				}
			}

			if(!empty($response)){
				$this->success("Records found !",$response);
			}else{
				$this->error("Records not found !");
			}



		}





		public function postMySongs(Request $request){
		/*print_r($request->user_id);
		die();*/
		$this->validation($request->all(), [
			"user_id" => "required",
			"songs_title" => "required",
			"cat_id"=>"required",
		]);
		$image='';
		$duration=0;
		$songFile='';
		$favrite=0;
		$request->songs_description='';


		if($request->file('songs')){
            //$imageName = time().'.'.$request->images->getClientOriginalExtension();

			$songFile = $this->uploadS3Data(@$request->file('songs'), 'mySongs');
			$data['songs']= $songFile;
			$data['songs_duration']=$duration;

           /* $request->images->move(public_path('/images'), $imageName);
           $input['image'] = $imageName;*/
       }


       /* if($request->file('songs')){
         	$audio = new Mp3Info($request->file('songs'), true);
         	$duration = (int) ($audio->duration)*1000;
         	$songFile = $this->uploadS3Data(@$request->file('songs'), 'mySongs');
         }*/

         /*print_r($songFile);
         die();*/

         if($request->favrite!=0){
         	$favrite=$request->favrite;
         }else{
         	$favrite=0;
         }
         $data['user_id']=$request->user_id;
         $data['cat_id']= $request->cat_id;
         $data['songs_images']=$image;
         $data['songs_title']=$request->songs_title;
         $data['favrite_status']=$favrite;
         $data['songs_description']=$request->songs_description;
         /*$data = [
         	'user_id' => $request->user_id,
         	'cat_id' => $request->cat_id,
         	'songs_images'      => $image,
         	'songs'       => $songFile,
         	'songs_duration'   => $duration,
         	'songs_title'   => $request->songs_title,
         	'favrite_status'   => $favrite,
         	'songs_description'=>$request->songs_description
         ];*/



         if($request->songs_id==''){
         	DB::table('mysongs')->insert($data);
         }else{
         	DB::table('mysongs')->where('id',$request->songs_id)->update($data);
         }
         
         $file_path1 = $this->getPath(5);
         $url1 = Storage::disk('s3')->url($file_path1);

         $file_path = $this->getPath(2);
         $url = Storage::disk('s3')->url($file_path);
         $response=DB::table('contents')->select('contents.*','mysongs.*')
         ->join('mysongs','mysongs.cat_id','contents.id')->where('mysongs.user_id',$request->user_id)->where('contents.id',$request->cat_id)->get();
         if($response){
         	foreach($response as $row){
         		$row->image=$url.'/'.$row->image;
         		if($row->songs==''){
         			$row->upload_status=0;
         			//$row->image=$url.'/'.$row->image;
         			$row->notuploadrecording=$url.'/'.$row->songs_not_upload;
         		}else{
         			$row->upload_status=1;
         			$row->uploadrecording=$url.'/'.$row->songs_upload;
         		}
         		$row->songs=$url1.'/'.$row->songs;
         	}
         	$this->success('Affirmation recording saved',$response);
         }else{
         	$this->error('My songs is not found!');
         }

     }


     public function myfavoritesongs(Request $request){
     	$this->validation($request->all(), [
     		"user_id" => "required",
     	]);
     	$file_path = $this->getPath(2);
     	$url = Storage::disk('s3')->url($file_path);
     	$file_path1 = $this->getPath(5);
     	$url1 = Storage::disk('s3')->url($file_path1);

     	$songs = Storage::disk('s3')->url('songs');
     	$playall=array();
     	$records['playAll']=DB::table('session_favourites')->where('favourite_userid',$request->user_id)->where('favourite_status',1)->get();
     	foreach($records['playAll'] as $ft){
     		$sessions=DB::table('affirmation')->where('id',$ft->favourite_session_id)->first();
     		$myfav=$songs.'/'.$sessions->affirmation_songs;
     		array_push($playall, $myfav);
     	}


     	$response['playall']=$playall;

     	$catgroy=array();
     	$records=DB::table('session_favourites')->select('affirmation.cat_id')->join('affirmation','affirmation.id','session_favourites.favourite_session_id')->where('favourite_userid',$request->user_id)->where('session_favourites.favourite_status',1)->groupBy('affirmation.cat_id')->get();
     	foreach($records as $ft){
     		array_push($catgroy,$ft->cat_id);
     	}

     	$response['categories']=Content::where('cat_id',2)->whereIn('id',$catgroy)->get();
     	foreach($response['categories'] as  $key => $row){
     		$row->image=$url.'/'.$row->image;

     		$counts=DB::table('session_favourites')->select('affirmation.*')->join('affirmation','affirmation.id','session_favourites.favourite_session_id')->where('favourite_userid',$request->user_id)->where('affirmation.cat_id',$row->id)->where('session_favourites.favourite_status',1)->count();

     		//$counts=DB::table('mysongs')->where('user_id',$request->user_id)->where('favrite_status',1)->where('cat_id',$row->id)->count();



     		$row->count=$counts;
     		$response['categories'][$key]['session']=DB::table('session_favourites')->select('affirmation.*','session_favourites.*')->join('affirmation','affirmation.id','session_favourites.favourite_session_id')->where('favourite_userid',$request->user_id)->where('affirmation.cat_id',$row->id)->where('session_favourites.favourite_status',1)->get();
     		foreach($response['categories'][$key]['session'] as $rows){
     			$myfav=$songs.'/'.$sessions->affirmation_songs;
     		}
     	}



     	$this->success('my favourite songs',$response);



     }


     public function musicPlayer(Request $request){
     	$this->validation($request->all(), [
     		"user_id" => "required",
     	]);
     	$response['nature']=DB::table('nature')->where('nature_status',1)->get();
     	foreach($response['nature'] as $nature){
     		$nature->songs=asset('public/voice').'/'.$nature->nature_sound;
     		$nature->icon=asset('public/images/flag').'/'.$nature->icon;
     	}
     	$voice=DB::table('voice')->where('status',1)->get();
     	if(!$voice->isEmpty()){
     		foreach($voice as $ft){
     			$ft->image=asset('public').'/images/'.$ft->image;
     			$ft->flag=asset('public').'/images/flag/'.$ft->flag;
     			$ft->voices=asset('public').'/voice/'.$ft->voices;
     		

     	if($request->session_id!=''){
     	$affirmationCategoies=DB::table('affirmation')->where('id',$request->session_id)->first();
     	$voiceId=$affirmationCategoies->artist_id;
     	// $affirmationCategoies->cat_id;
     	
     	$picture1 = Storage::disk('s3')->url('songPic');
		$songs = Storage::disk('s3')->url('songs');
		//DB::enableQueryLog();
     	$response1=DB::table('affirmation')->where('cat_id',$affirmationCategoies->cat_id)->where('affirmation_title',$affirmationCategoies->affirmation_title)->where('affirmation_subtitle',$affirmationCategoies->affirmation_subtitle)->where('artist_id',$ft->id)->get();
     	//dd(DB::getQueryLog());
     	foreach($response1 as $Music){

			$date=User::where('id',$request->user_id)->first();
			$datetime1 = date_create(date('d-m-Y',strtotime($date->updated_at))); 
			$datetime2 = date_create(date('d-m-Y')); 
			$interval = date_diff($datetime1, $datetime2); 
			$day=$interval->format('%a'); 
			if($day < 90){
				$ft->images=$picture1.'/'.$Music->affirmation_images;
				$ft->lockUnlockStatus=1;
				$ft->title=$Music->affirmation_title;
			}else{

				$subscripion=DB::table('userPayments')->where('payment_users',$request->user_id)->where('subscription','subscription')->orderBy('id','desc')->limit(1)->first();

				if(!empty($subscripion)){
					if($subscripion->package_type==1){
						$subscripionDate=strtotime($subscripion->payment_date.'+ 1 months');
						if($subscripionDate > strtotime(date('Y-m-d'))){
							$ft->images=$picture1.'/'.$Music->affirmation_images;
							$ft->lockUnlockStatus=1;
							$ft->title=$Music->affirmation_title;
						}else{
							$ft->images=$picture1.'/'.$Music->affirmation_images;
							$ft->lockUnlockStatus=0;
							$ft->title=$Music->affirmation_title;
						}
					}elseif($subscripion->package_type==2){
						$subscripionDate=strtotime($subscripion->payment_date.'+ 12 months');
						if($subscripionDate > strtotime(date('Y-m-d'))){
							$ft->images=$picture1.'/'.$Music->affirmation_images;
							$ft->lockUnlockStatus=1;
							$ft->title=$Music->affirmation_title;
						}else{
							$ft->images=$picture1.'/'.$Music->affirmation_images;
							$ft->lockUnlockStatus=0;
							$ft->title=$Music->affirmation_title;
						}
					}else{
						$count=DB::table('userPayments')->where('payment_users',$request->user_id)->where('payment_plan_id',$Music->id)->count();
						if($count>0){
							$ft->images=$picture1.'/'.$Music->affirmation_images;
							$ft->lockUnlockStatus=1;
							$ft->title=$Music->affirmation_title;;
						}else{
							$ft->images=$picture1.'/'.$Music->affirmation_images;
							$ft->lockUnlockStatus=0;
							$ft->title=$Music->affirmation_title;
						}
					}
				}else{
					$count=DB::table('userPayments')->where('payment_users',$request->user_id)->where('payment_plan_id',$Music->id)->count();
					if($count>0){
						$ft->images=$picture1.'/'.$Music->affirmation_images;
						$ft->lockUnlockStatus=1;
						$ft->title=$Music->affirmation_title;
					}else{
						$ft->images=$picture1.'/'.$Music->affirmation_images;
						$ft->lockUnlockStatus=0;
						$ft->title=$Music->affirmation_title;
					}
				}
			}		

			$ft->songs=$songs.'/'.$Music->affirmation_songs;

		}
		
	}
	}
     		$response['getVoice']=$voice;
     	}
		

     	$this->success('record found.', $response);
     }


     public function searchCategory(Request $request){
     	$this->validation($request->all(), [
     		"user_id" => "required",
     		"title" => "required",
     	]);
     	$file_path = $this->getPath('2');
     	$url = Storage::disk('s3')->url($file_path);
     	DB::enableQueryLog();	
     	$response['random']=Content::where('cat_id',2)->where('name','like',"%{$request->title}%")->inRandomOrder()->first();
	//dd(DB::getQueryLog()); 
     	if(!empty($response['random'])){
     		foreach ($response as $key => $ft) {
     			$ft->image=$url.'/'.$ft->banner;
     		}
     	}

     	$response['interested']=array();
     	$file_path = $this->getPath('2');
     	$interest=DB::table('callect_category')->where('callect_userid',$request->user_id)->get();
     	foreach($interest as $ft){
     		$category_id=json_decode($ft->callect_categoryid);
     		if(!empty($category_id)){
     			$response['interested']=DB::table('contents')->whereIn('id',$category_id)->where('name','like',"%{$request->title}%")->where('status',1)->get();
     			foreach($response['interested'] as $catid){
     				$catid->image=$url.'/'.$catid->image;
     				$catid->user_id=$ft->callect_userid;
     			}
     		}
     	}




     	$file_path = $this->getPath('2');
     	$response['categories']=DB::table('contents')->where('cat_id','2')->where('name','like',"%{$request->title}%")->where('status',1)->get();
     	if($response['categories']->isEmpty()){
     		$this->error('record not found.');
     	}else{
     		foreach ($response['categories'] as $key => $value) {
     			$value->image=$url.'/'.$value->image;
     		}
     	}

     	$array[]=array(
     		'id'=>1,
     		'type'=>'My Recordings',
     		'date'=>date('d-m-Y'),
     	);

     	$array[]=array(
     		'id'=>1,
     		'type'=>'My Favorites',
     		'date'=>date('d-m-Y'),
     	);

     	$response['mystuff']=$array;

     	$response['nature']=DB::table('nature')->where('nature_status',1)->where('nature_name','like',"%{$request->title}%")->where('music_type','Nature')->get();
     	foreach($response['nature'] as $nature){

     		$results=User::where('id',$request->user_id)->first();
     		$datetime1 = date_create(date('d-m-Y',strtotime($results->updated_at))); 
     		$datetime2 = date_create(date('d-m-Y'));
     		$interval = date_diff($datetime1, $datetime2); 
     		$day=$interval->format('%a'); 

     		if($day < 90){
     			$nature->images=asset('public/images/flag').'/'.$nature->nature_image;
     			$nature->lockUnlockStatus=1;
     		}else{

     			$subscripion=DB::table('userPayments')->where('payment_users',$request->user_id)->where('subscription','subscription')->orderBy('id','desc')->limit(1)->first();
					//print_r($subscripion);
     			if(!empty($subscripion)){
     				if($subscripion->package_type==1){
     					$subscripionDate=strtotime($subscripion->payment_date.'+ 1 months');
     					if($subscripionDate > strtotime(date('Y-m-d'))){
     						$nature->images=asset('public/images/flag').'/'.$nature->nature_image;
     						$nature->lockUnlockStatus=1;
     					}else{
     						$nature->images=asset('public/images/flag').'/'.$nature->lock_images;
     						$nature->lockUnlockStatus=0;
     					}

     				}elseif($subscripion->package_type==2){
     					$subscripionDate=strtotime($subscripion->payment_date.'+ 12 months');
     					if($subscripionDate > strtotime(date('Y-m-d'))){
     						$nature->images=asset('public/images/flag').'/'.$nature->nature_image;
     						$nature->lockUnlockStatus=1;
     					}else{
     						$nature->images=asset('public/images/flag').'/'.$nature->lock_images;
     						$nature->lockUnlockStatus=0;
     					}
     				}else{
     					$count=DB::table('userPayments')->where('payment_users',$request->user_id)->where('payment_plan_id',$nature->nature_id)->count();
     					if($count>0){
     						$nature->images=asset('public/images/flag').'/'.$nature->nature_image;
     						$nature->lockUnlockStatus=1;
     					}else{
     						$nature->images=asset('public/images/flag').'/'.$nature->lock_images;
     						$nature->lockUnlockStatus=0;
     					}
     				}
     			}else{
     				$count=DB::table('userPayments')->where('payment_users',$request->user_id)->where('payment_plan_id',$nature->nature_id)->count();
     				if($count>0){
     					$nature->images=asset('public/images/flag').'/'.$nature->nature_image;
     					$nature->lockUnlockStatus=1;
     				}else{
     					$nature->images=asset('public/images/flag').'/'.$nature->lock_images;
     					$nature->lockUnlockStatus=0;
     				}
     			}


     		}


     		$nature->songs=asset('public/voice').'/'.$nature->nature_sound;

     	}

     	$this->success('record found.', $response);

     }


     public function searchSongs(Request $request){
     	$this->validation($request->all(), [
     		"user_id" => "required",
     		"title" => "required",
     	]);
     	$response['SoundScopes']=DB::table('nature')->where('nature_name','like',"%{$request->title}%")->where('nature_status',1)->where('music_type','Nature')->get();
     	foreach($response['SoundScopes'] as $nature){
     		$count=DB::table('userPayments')->where('payment_users',$request->user_id)->where('payment_plan_id',$nature->nature_id)->count();
     		if($count>0){
     			$nature->images=asset('public/images/flag').'/'.$nature->nature_image;
     			$nature->lockUnlockStatus=1;
     		}else{
     			$nature->images=asset('public/images/flag').'/'.$nature->lock_images;
     			$nature->lockUnlockStatus=0;
     		}

     		$nature->songs=asset('public/voice').'/'.$nature->nature_sound;

     	}

     	$response['Music']=DB::table('nature')->where('nature_name','like',"%{$request->title}%")->where('nature_status',1)->where('music_type','Music')->get();
     	foreach($response['Music'] as $Music){
     		$count=DB::table('userPayments')->where('payment_users',$request->user_id)->where('payment_plan_id',$Music->nature_id)->count();
     		if($count>0){
     			$Music->images=asset('public/images/flag').'/'.$Music->nature_image;
     			$Music->lockUnlockStatus=1;

     		}else{
     			$Music->images=asset('public/images/flag').'/'.$Music->lock_images;
     			$Music->lockUnlockStatus=0;
     		}
     		$Music->songs=asset('public/voice').'/'.$Music->nature_sound;

     	}
     	$this->success('record found.', $response);

     }

     public function is_notification(Request $request){
     	$this->validation($request->all(), [
     		"user_id" => "required",
     		"status" => "required",
     	]);
     	if(User::where('id',$request->user_id)->update(array('is_notification'=>$request->status))){
     		if($request->status==1){
     			$this->success('Successfuly on notification',array('status'=>$request->status));
     		}elseif($request->status==0){
     			$this->success('Successfuly off notification',array('status'=>$request->status));
     		}
     	}else{
     		$this->error('notification status not update');
     	}


     }


     public function deleteRecording(Request $request){
     	$this->validation($request->all(), [
     		"user_id" => "required",
     		"recording_id" => "required",
     	]);

     	if(DB::table('mysongs')->where('user_id',$request->user_id)->where('id',$request->recording_id)->delete()){
     		$this->success('Successfully delete recording');
     	}else{
     		$this->error('Recording not found!');
     	}

     }

     public function notificationlist(Request $request){
     	$this->validation($request->all(), [
     		"user_id" => "required",
     	]);

     	$array=array();
     	$results=DB::table('notifications')->get();		
     	foreach($results as $ft){
     		if($ft->user_id==$request->user_id){
     			$message=json_decode($ft->message);
     			$array[]=$message;
     		}
     		if($ft->type=='admin'){
     			$message=json_decode($ft->message);
     			$array[]=$message;
     		}
     	}

     	$this->success('records found',$array);

     }

     public function editVoice(Request $request){
     	$this->validation($request->all(), [
     		"user_id" => "required",
     		"voice_id" => "required",
     	]);
     	if($request->time!=''){
     		$data['user_time']=date('H:i:s',strtotime($request->time));
     	}
     	if($request->status!=''){
     		$data['on_off_status']=$request->status;
     	}


     	DB::table('user_voice')->where('user_voice_id',$request->voice_id)->update($data);

     	$record=DB::table('user_voice')->where('user_voice_id',$request->voice_id)->where('user_id',$request->user_id)->get();
     	$this->success("record found",$record);
     }


     public function getUserVoiceList(Request $request){
     	$this->validation($request->all(), [
     		"user_id" => "required",
     	]);
     	$record=DB::table('user_voice')->where('user_id',$request->user_id)->limit(1)->get();
     	if(!$record->isEmpty()){
     		foreach($record as $row){
     			$row->voice_id=json_decode($row->voice_id);
     			$row->user_time=date('H:i:a',strtotime($row->user_time));
     		}
     		$this->success("record found",$record);
     	}else{
     		$this->error("record not found");
     	}

     }


     public function sendNotificationCronjobs(Request $request){


     	$array=array();
     	DB::enableQueryLog(); 
     	$record=DB::table('user_voice')->where('on_off_status',1)->where('user_time','Like',date('H:i').'%')->get();
     	echo date('H:i');
     	if(!$record->isEmpty()){
     		foreach($record as $row){
     			$users=$row->user_id;
     			array_push($array,$users);
     		}
    
     		$payload = [
     			'title' =>'Songs', 
     			'body'  => 'Just a friendly reminder to listen to some amazing affirmations. You deserve it!', 
     			'value' => '',
     			'icon' =>asset('public/img/admin/fb.png'),    
     			'sound' => 'default', 
     			'priority' => 'high',
     			'type'  => 2
     		];
     		$mydata =   $this->sendNotificationGroupUsers($user = [], $payload,$array);
             print_r($mydata);

     	}	
     }

     public function checkVoice(Request $request)
     {
     	$this->validation($request->all(), [
     		"user_id" => "required",
     	]);

     	$response=DB::table('user_voice')->where('user_id',$request->user_id)->orderBy('user_voice_id','desc')->limit(1)->get();
     	foreach($response as $ft){
     			$filter=json_decode($ft->voice_id);
     			if($filter[0]==1){
     				$ft->voice_gendor=0;
     			}else{
     				$ft->voice_gendor=1;
     			}
     		//$ft->voice=$ft->voice_id;
     	}

     	$this->success('Records found !',$response);
     }



     public function favouriteSession(Request $request){
     	$this->validation($request->all(), [
     		"user_id" => "required",
     		"session_id"=>"required"
     	]);
     	
     	$record=DB::table('session_favourites')->where('favourite_session_id',$request->session_id)->where('favourite_userid',$request->user_id)->first();
     	if(!empty($record)){
     			if($record->favourite_status==1){
     				$array=array(
     					'favourite_session_id'=>$request->session_id,
     					'favourite_userid'=>$request->user_id,
     					'favourite_status'=>0
     					);
     				DB::table('session_favourites')->where('favourite_session_id',$request->session_id)->where('favourite_userid',$request->user_id)->update($array);
     	
     			}else{
     				$array=array(
     					'favourite_session_id'=>$request->session_id,
     					'favourite_userid'=>$request->user_id,
     					'favourite_status'=>1
     					);
     				DB::table('session_favourites')->where('favourite_session_id',$request->session_id)->where('favourite_userid',$request->user_id)->update($array);
     			}
     	}else{
     			$array=array(
     					'favourite_session_id'=>$request->session_id,
     					'favourite_userid'=>$request->user_id
     					);
     	DB::table('session_favourites')->insert($array);
     	}
     	$songs = Storage::disk('s3')->url('songs');
     	$response['favourite']=DB::table('session_favourites')->select('affirmation.*','session_favourites.*')->join('affirmation','affirmation.id','session_favourites.favourite_session_id')->where('favourite_userid',$request->user_id)->where('affirmation.id',$request->session_id)->get();

     		foreach($response['favourite'] as $rows){
     			$rows->affirmation_songs=$songs.'/'.$rows->affirmation_songs;
     		}
     	$this->success('Successfuly add your favourite',$response);
     }


     public function userNotification(){
     	echo 1;
     }

// End controller

 }
