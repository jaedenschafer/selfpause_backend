<?php

use Illuminate\Http\Request;


Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
    return "Cache is cleared";
});

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['namespace' => 'Api'], function () {

  //auth process
  Route::post('auth/register','AuthController@register');
  Route::post('auth/login','AuthController@login');
   Route::post('auth/socailLogin','AuthController@socailLogin');
  Route::post('auth/forgotPassword','AuthController@forgotPassword');
  Route::post('auth/changePassword','AuthController@changePassword');
  Route::post('auth/getProfile','AuthController@getProfile');
  Route::post('auth/editProfile','AuthController@editProfile');
  Route::post('auth/changeNotification','AuthController@changeNotification');
  Route::post('auth/logout','AuthController@logout');

  Route::post('auth/checkSubscription','AuthController@checkSubscription');


  Route::get('auth/testNotification','AuthController@testNotification');
  Route::post('collections/myRecording','CollectionsController@myRecording');
  //Voice select
    Route::post('collections/getVoice','CollectionsController@getVoice');
    Route::post('collections/setVoice','CollectionsController@setVoice');

    Route::post('collections/support','CollectionsController@support');
    Route::post('collections/help','CollectionsController@help');

    Route::post('collections/postMySongs','CollectionsController@postMySongs');
    Route::post('collections/deleteRecording','CollectionsController@deleteRecording');
    Route::post('collections/notificationlist','CollectionsController@notificationlist');

  Route::post('collections/setContentsInfo','CollectionsController@setContentsInfo');

  //Collections
  Route::post('collections/getHomeData','CollectionsController@getHomeData');
  Route::get('collections/getTracks','CollectionsController@getTracks');
  Route::post('collections/getAllSongs','CollectionsController@getAllSongs');
  Route::post('collections/getSongsByGroup','CollectionsController@getSongsByGroup');
  Route::post('collections/getContentsInfo','CollectionsController@getContentsInfo');
  Route::post('collections/getCategoryes','CollectionsController@getCategoryes');

  Route::post('collections/searchCategory','CollectionsController@searchCategory');
  Route::post('collections/searchSongs','CollectionsController@searchSongs');
  //Favourite
  Route::post('collections/getVoice','CollectionsController@getVoice');

// Home page Category and fann_randomize_weights
Route::post('collections/randomCategory','CollectionsController@randomCategory');

 Route::post('collections/myfavoritesongs','CollectionsController@myfavoritesongs');
  Route::post('collections/musicPlayer','CollectionsController@musicPlayer');
  Route::post('collections/is_notification','CollectionsController@is_notification');

  //Callect category

  Route::post('collections/collectCategory','CollectionsController@collectCategory');
  Route::post('collections/mySongsList','CollectionsController@mySongsList');
  Route::post('collections/music','CollectionsController@music');
  Route::post('collections/affirmationCategoies','CollectionsController@affirmationCategoies');


  //Explore Data
  Route::post('collections/getExploreData','CollectionsController@getExploreData');
  Route::post('collections/getExploreNewInfo','CollectionsController@getExploreNewInfo');
  Route::post('collections/getExploreTopInfo','CollectionsController@getExploreTopInfo');
  Route::post('collections/getExploreTopInfo','CollectionsController@getExploreTopInfo');

  //top view all
  Route::post('collections/viewAllPopularSongs','CollectionsController@viewAllPopularSongs');
  Route::post('collections/viewAllPopularPlaylist','CollectionsController@viewAllPopularPlaylist');

  //
  Route::post('collections/getGenresMood','CollectionsController@getGenresMood');

  //My Collections
  Route::post('mycollections/getPlaylist','MyCollectionsController@getPlaylist');
  Route::post('mycollections/getAlbums','MyCollectionsController@getAlbums');
  Route::post('mycollections/getArtists','MyCollectionsController@getArtists');
  Route::post('mycollections/getSongs','MyCollectionsController@getSongs');
  Route::post('mycollections/addStats','MyCollectionsController@addStats');

  //my collection my playlist
  Route::post('mycollections/createPlaylist','MyCollectionsController@createPlaylist');
  Route::post('mycollections/getMyPlaylist','MyCollectionsController@getMyPlaylist');
  Route::post('mycollections/addSongPlaylist','MyCollectionsController@addSongPlaylist');
  Route::post('mycollections/getMyPlaylistSong','MyCollectionsController@getMyPlaylistSong');

  //search all data
  Route::post('mycollections/searchAllData','MyCollectionsController@searchAllData');
  Route::post('mycollections/searchViewAll','MyCollectionsController@searchViewAll');

  //payment modules
  Route::post('payment/userPayment','PaymentsController@userPayment');
  Route::post('payment/subscriptions','PaymentsController@usersubscriptions');

  Route::post('payment/generateToken','PaymentsController@token');


/*BrainTree PaymentGateway*/
  Route::post('payment/brainTreegenerateToken','PaymentsController@brainTreegenerateToken');

  Route::post('payment/brainTreePayment','PaymentsController@brainTreePayment')->name('payment.make');

  Route::post('payment/brainTreesignup','PaymentsController@brainTreesignup');

/*End */
  
  Route::post('payment/payment','PaymentsController@payment');

  //Web hook from stripe for subscription
  Route::post('payment/webhookSubscription','PaymentsController@webhookSubscription');
  Route::post('payment/cancelSubscription','PaymentsController@cancelSubscription');


  Route::post('payment/sendIam','PaymentsController@sendIam');
  Route::post('payment/sendImHistory','PaymentsController@sendImHistory');
  Route::post('payment/receiveImHistory','PaymentsController@receiveImHistory');
  Route::post('payment/requestIam','PaymentsController@requestIam');



  Route::get('auth/getTracks','AuthController@getTracks');
  Route::post("music/getPlaylist",'AuthController@getPlaylists');
  Route::post('auth/getTracksByArtist','AuthController@getTracksByArtist');
  Route::post('auth/getTracksByAlbum','AuthController@getTracksByAlbum');

  //Help pages
  Route::get('auth/aboutUs','AuthController@aboutUs');
  Route::get('auth/privacyPolicy','AuthController@privacyPolicy');
  Route::get('auth/termsCondtions','AuthController@termsCondtions');


  Route::post('auth/contactUs','AuthController@contactUs');
  Route::post('auth/deleteKeyword','AuthController@deleteKeyword');
  Route::get('auth/confirmMail','AuthController@confirmMail');
  Route::post('auth/resendVerificationmail','AuthController@resendVerificationmail');
  Route::post('auth/changeNotification','AuthController@changeNotification');

  //update qb id
  Route::post('auth/updateQbId','AuthController@updateQbId');
  Route::post('auth/getQbDetails','AuthController@getQbDetails');

  //Referral
  Route::post('auth/AddImByReferral','AuthController@AddImByReferral');



  //statement modules
  Route::get('statement/getVerbs','StatementController@getVerbs');
  Route::post('statement/submitStatement','StatementController@submitStatement');
  Route::post('statement/getStatements','StatementController@getStatements');
  Route::post('statement/myStatements','StatementController@myStatements');
  Route::post('statement/editStatement','StatementController@editStatement');
  Route::post('statement/deleteStatement','StatementController@deleteStatement');

  //payment modules
  // Route::get('payment/generateToken','PaymentsController@token');
  // Route::post('payment/byIam','PaymentsController@payment');
  // Route::post('payment/sendIam','PaymentsController@sendIam');
  // Route::post('payment/sendImHistory','PaymentsController@sendImHistory');
  // Route::post('payment/receiveImHistory','PaymentsController@receiveImHistory');
  // Route::post('payment/requestIam','PaymentsController@requestIam');

  // Route::get('users/searchUsers','PaymentsController@searchUsers');
  // Route::post('users/getNotifications','PaymentsController@getNotifications');


  Route::get('/posts', 'PostsController@index');
  Route::resource('/users', 'UsersController');
/*---------------------------------------------------------------------------------------------*/

 Route::post('collections/getUserVoiceList','CollectionsController@getUserVoiceList');
 Route::post('collections/editVoice','CollectionsController@editVoice');
Route::get('collections/sendNotificationCronjobs','CollectionsController@sendNotificationCronjobs');

 Route::post('collections/checkVoice','CollectionsController@checkVoice');
 Route::post('collections/setArtist','CollectionsController@setArtist');
 Route::post('collections/favouriteSession','CollectionsController@favouriteSession');

 Route::get('collections/userNotification','CollectionsController@userNotification');




});
