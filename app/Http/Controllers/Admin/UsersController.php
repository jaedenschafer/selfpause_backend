<?php

namespace App\Http\Controllers\Admin;

use App\Users;
use App\Transaction;
use App\Favourite;
use App\Http\Controllers\Api\StringcompareController;
use Illuminate\Http\Request;
use URL;
use DB;
use Storage;
use App\UserPayment;
class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $usres = [];
       $users = Users::where('id', '<>', 1)->orderBy('id', 'DESC')->get();

       return view('admin.users.index', compact('users'));
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     * @return type 1=> artist, 2=> album, 3=> playlist, 4=> song
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function getPath($id='') {
        if ($id == 1) {

            $file_path = 'artists';
        }elseif ($id == 2) {

            $file_path = 'albums';
        }elseif ($id == 3) {

            $file_path = 'playlists';
        }elseif ($id == 4) {

            $file_path = 'tracks';
        }
        elseif ($id == 5) {

            $file_path = 'mySongs';
        }
        return $file_path;
    }
    public function show(Request $request)
    {
         $id=$request->segment(3);
     
       $users_details = Users::where(['id'=>$id])->first();

            $users_details->PaymentStatus=UserPayment::where('payment_users',$users_details->id)->whereNotNull('subscription')->orderBy('id','DESC')->limit(1)->count();



      /* $fav_albums =  Favourite::where(['type'=>2, 'user_id'=>$id, 'status'=>1])->with('getContentsInfo')->get();

       $fav_artists =  Favourite::where(['type'=>1, 'user_id'=>$id, 'status'=>1])->with('getContentsInfo')->get();*/
        $file_path1 = $this->getPath(5);
        $url1 = Storage::disk('s3')->url($file_path1);

        $file_path = $this->getPath(2);
        $url = Storage::disk('s3')->url($file_path);

       // $fav_playlists =  Favourite::where(['type'=>3, 'user_id'=>$id, 'status'=>1])->with('getContentsInfo')->get();

        $fav_playlists=DB::table('contents')->select('contents.*','mysongs.*')
        ->join('mysongs','mysongs.cat_id','contents.id')->where('mysongs.user_id',$id)->where('favrite_status',1)->get();
            foreach($fav_playlists as $ft){
                 $ft->image=$url.'/'.$ft->image;
                if($ft->songs==''){
                    $ft->upload_status=0;
                }else{
                    $ft->upload_status=1;
                }
                $ft->songs=$url1.'/'.$ft->songs;
               
        }

        $fav_songs=DB::table('contents')->select('contents.*','mysongs.*')
        ->join('mysongs','mysongs.cat_id','contents.id')->where('mysongs.user_id',$id)->get();


        foreach($fav_songs as $row){
                $row->image=$url.'/'.$row->image;
                if($row->songs==''){
                    $row->upload_status=0;
                }else{
                    $row->upload_status=1;
                }
                $row->songs=$url1.'/'.$row->songs;
            }


       //$fav_songs =  Favourite::where(['type'=>4, 'user_id'=>$id, 'status'=>1])->with('getSongInfo')->get();

       return view('admin.users.view', compact('users_details', 'fav_playlists', 'fav_songs'));
    }

    //get user profile using from user_id
    public static function getUserProfile($user_id='') {

        $userinfo =  Users::where('id', $user_id)->first();
        $picture = URL::to('/uploads/').'/'.'images.jpg';
        if ($userinfo) {
            if(strstr($userinfo->profile, 'https://')){
                $picture = $userinfo->profile;
            }elseif(!empty($userinfo->profile)){
                $picture = URL::to('/uploads/users/').'/'.$userinfo->profile;
            }
        }
        return (string) @$picture;
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $bfaccounts = Users::findOrFail($id);
        $bfaccounts->delete();
         return redirect('admin/users');
    }


    public function applyuserplans(Request $request){
        $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $autogenerate= substr(str_shuffle($permitted_chars), 0, 30);

        if($request->types=='Free'){
                 $array=array(
                        'payment_users'=>$request->userid,
                        'payment_type'=>'Pay by admin',
                        'payment_id'=>$autogenerate,
                        'payment_amount'=>0,
                        'payment_date'=>date('Y-m-d H:i:s'),
                        'payment_status'=>1,
                        'package_type'=>1,
                        'subscription'=>NULL,
        ) ; 
        }else{
             $array=array(
                        'payment_users'=>$request->userid,
                        'payment_type'=>'Pay by admin',
                        'payment_id'=>$autogenerate,
                        'payment_amount'=>0,
                        'payment_date'=>date('Y-m-d H:i:s'),
                        'payment_status'=>1,
                        'package_type'=>1,
                        'subscription'=>'subscription',
        ) ; 
        }   

        if(UserPayment::where('payment_users',$request->userid)->count('id') > 0){
                $lastRecors=UserPayment::where('payment_users',$request->userid)->orderBy('id','desc')->limit(1)->first();
                UserPayment::where('payment_users',$request->userid)->where('id',$lastRecors->id)->update($array);
        }else{
                UserPayment::insert($array);
        }     
    }

}
