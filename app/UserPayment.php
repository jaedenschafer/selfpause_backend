<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPayment extends Model
{
    protected $table='userPayments';
    protected $guarded=[];    
}
