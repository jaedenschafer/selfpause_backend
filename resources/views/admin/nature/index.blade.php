@extends('layouts.admin')



@section('content')

<div class="container-fluid">

    <br />



     <a href="{{ url('/admin/addNature') }}" title="Back"><button class="btn btn-success btn-sm">  <i class="fa fa-plus" aria-hidden="true"></i> Add New </button></a>

 <h4 class="title">Sounds</h4>

 <table  class="table table-striped table-no-bordered table-hover" width="100%" style="width:100%">

    <thead>

        <tr>

            <th>Id</th>

            <th>Name</th>

            <th>Songs</th>

            <th>Unlock Image</th>
            <th>locked Image</th>

            <th>Date</th>

            <th>Action</th>

        </tr>

    </thead>

    <tbody>
        @foreach($response as $ft)
            <tr>
                <td>{{$ft->nature_id}}</td>
                <td>{{$ft->nature_name}}</td>
                <td>{{$ft->nature_sound}}</td>
                <td><img src="{{asset('public/images/flag/'.$ft->nature_image)}}" style="width: 100px"></td>
                <td><img src="{{asset('public/images/flag/'.$ft->lock_images)}}" style="width: 100px"></td>
                <td>{{$ft->nature_date}}</td>

                <td>

                    <a href="{{ url('/admin/addNature',['key'=>$ft->nature_id]) }}" class="btn btn-info sm-btn">Edit</a>

                    <button type="submit" class="btn btn-danger btn-sm" title="Delete Song" onclick="DeleteNature({{$ft->nature_id}})"><i class="fa fa-trash-o" aria-hidden="true"></i> </button>

                </td>
            </tr>
        @endforeach

    </tbody>

</table>

</div>
<script type="text/javascript">
    function DeleteNature(id){
        const parmt={
            _token:"{{csrf_token()}}",
            id:id
        }
        if(confirm('Confirm delete?')){
        $.post('{{route("natures")}}',parmt).then(function(response){
            window.location.href='';
        })
        }

    }
</script>
@endsection

