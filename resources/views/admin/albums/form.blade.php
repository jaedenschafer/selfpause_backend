<?php 
use App\Http\Controllers\Controller;
?>

<input class="form-control" name="cat_id" type="hidden" id="cat_id" value="{{ 2 }}" >

<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    <label for="name" class="control-label">{{ 'Title' }}</label>
    <input class="form-control" name="name" type="text" id="name" value="{{ $content->name or ''}}" required />
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('subtitle') ? 'has-error' : ''}}">
    <label for="subtitle" class="control-label">{{ 'Subtitle' }}</label>
    <input class="form-control" name="subtitle" type="text" id="subtitle" value="{{ $content->subtitle or ''}}" required />
    {!! $errors->first('subtitle', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
    <label for="description" class="control-label">{{ 'Description' }}</label>
    <textarea class="form-control" rows="5" name="description" type="textarea" id="description" >{{ $content->description or ''}}</textarea>
    {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
    <label for="image" class="control-label">{{ 'Image' }}</label>

    <?php
    if(!empty($content->image)) {
        $picture = Controller::getFilePath($content->image, 'albums');
        if($picture) {
            ?>
            <img src="{{ $picture }}" height="150px" width="150px">
            <?php
        }
    }
    ?>
    <input class="" name="image" type="file" id="image" value="{{ $content->image or ''}}" >
    {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('banner') ? 'has-error' : ''}}">
    <label for="image" class="control-label">{{ 'Banner' }}</label>

    <?php
    if(!empty($content->banner)) {
        $picture = Controller::getFilePath($content->banner, 'albums');
        if($picture) {
            ?>
            <img src="{{ $picture }}" height="150px" width="150px">
            <?php
        }
    }
    ?>
    <input class="" name="banner" type="file" id="banner" value="{{ $content->banner or ''}}" >
    {!! $errors->first('banner', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group {{ $errors->has('background_images') ? 'has-error' : ''}}">
    <label for="image" class="control-label">{{ 'Background' }}</label>

    <?php
    if(!empty($content->backgoround_images)) {
        $picture = Controller::getFilePath($content->backgoround_images, 'albums');
        if($picture) {
            ?>
            <img src="{{ $picture }}" height="150px" width="150px">
            <?php
        }
    }
    ?>
    <input class="" name="background_images" type="file" id="background_images" value="{{ $content->backgoround_images or ''}}" >
    {!! $errors->first('backgoround_images', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group {{ $errors->has('circular_images') ? 'has-error' : ''}}">
    <label for="image" class="control-label">{{ 'Circular Background' }}</label>

    <?php
    if(!empty($content->circular_images)) {
        $picture = Controller::getFilePath($content->circular_images, 'circular');
        if($picture) {
            ?>
            <img src="{{ $picture }}" height="150px" width="150px">
            <?php
        }
    }
    ?>
    <input class="" name="circular_images" type="file" id="circular_images" value="{{ $content->circular_images or ''}}" >
    {!! $errors->first('circular_images', '<p class="help-block">:message</p>') !!}
</div>








<p>Please select the identify images songs recording uploaded or not</p>


<div class="form-group {{ $errors->has('songs_upload') ? 'has-error' : ''}}">
    <label for="songsupload" class="control-label">{{ 'Sougs Upload images' }}</label>

    <?php
    if(!empty($content->songs_upload)) {
        $picture = Controller::getFilePath($content->songs_upload, 'albums');
        if($picture) {
            ?>
            <img src="{{ $picture }}" height="150px" width="150px">
            <?php
        }
    }
    ?>
    <input class="" name="songsupload" type="file" id="songsupload" value="{{ $content->banner or ''}}" >
    {!! $errors->first('banner', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('songs_not_upload') ? 'has-error' : ''}}">
    <label for="songsnotupload" class="control-label">{{ 'Songs not Upload images' }}</label>

    <?php
    if(!empty($content->songs_not_upload)) {
        $picture = Controller::getFilePath($content->songs_not_upload, 'albums');
        if($picture) {
            ?>
            <img src="{{ $picture }}" height="150px" width="150px">
            <?php
        }
    }
    ?>
    <input class="" name="songsnotupload" type="file" id="songsnotupload" value="{{ $content->banner or ''}}" >
    {!! $errors->first('banner', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
