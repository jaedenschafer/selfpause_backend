<?php use App\Http\Controllers\Controller;

use App\Http\Controllers\Admin\SongsController;

?>

<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    <label for="name" class="control-label">{{ 'Title' }}</label>
    <input class="form-control" name="name" type="text" id="name" value="{{ $song->affirmation_title or ''}}" required />
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('subtitle') ? 'has-error' : ''}}">
    <label for="subtitle" class="control-label">{{ 'Subtitle' }}</label>
    <input class="form-control" name="subtitle" type="text" id="subtitle" value="{{ $song->affirmation_subtitle or ''}}" >
    {!! $errors->first('subtitle', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
    <label for="description" class="control-label">{{ 'Description' }}</label>
    <textarea class="form-control" rows="4" cols="100" name="description" type="textarea" id="description" >{{ $song->affirmation_discription or ''}}</textarea>
    {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
    <label for="image" class="control-label">{{ 'Display Image' }}</label>
    <?php
    if(isset($song))
    {
        $getFile = Controller::getFilePath($song->affirmation_songs, 'songPic');
        if($getFile)
            { ?>
                <img height="200" width="200" src="{{ $getFile }}">
                <?php
            }
        } ?>
        <input class="form-control" name="image" type="file" id="image" value="{{ $song->affirmation_songs or ''}}" >
        {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
    </div>




    <?php 
    if(@$song!=''){
        @$response = SongsController::getSongsByUser(@$song->affirmation_title,@$song->affirmation_subtitle);
        if(!$response->isEmpty()){
            foreach($response as $ft){
                ?>

                <div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
                    <label for="song" class="control-label">{{ 'Song' }}</label>
                    @if(isset($song))
                    <input type="hidden" name="affirmation_id[]" value="{{$ft->id}}">
                    <input class="form-control" name="song[]" type="file" accept="audio/mpeg" id="song" value="{{ $ft->affirmation_songs or ''}}" {{ !empty($ft->affirmation_songs)?'':'required' }} /> {{ $ft->affirmation_songs }}
                    @else
                    <input class="form-control" name="song[]" type="file" id="song" accept="audio/mpeg" value="" required />
                    @endif
                    {!! $errors->first('song', '<p class="help-block">:message</p>') !!}
                </div>

                <!-- style="{{ isset($getArtist)?'':'display: none' }}" -->
                <div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
                    <label for="song" class="control-label">Select Artist</label>

                    <select class="form-control" id="artist_list" name="selectedArtist[]">
                        <option value="">Select Artist</option>

                        @foreach($artists as $key => $val)
                        @if(isset($song))
                        <option value="{{ $val->id}}" 
                            <?php if($val->id==$ft->artist_id) echo  'selected'; ?> >{{$val->name}}
                        </option>
                        @else
                        <option value="{{ $val->id}}">{{$val->name}}</option>
                        @endif
                        @endforeach
                    </select>
                </div>
                <?php
            }
        }

    }else{
        ?>
          <div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
                    <label for="song" class="control-label">{{ 'Song' }}</label>
                    @if(isset($song))
                    <input class="form-control" name="song" type="file" accept="audio/mpeg" id="song" value="{{ $song->affirmation_images or ''}}" {{ !empty($song->affirmation_images)?'':'required' }} /> {{ $song->affirmation_images }}
                    @else
                    <input class="form-control" name="song[]" type="file" id="song" accept="audio/mpeg" value="" required />
                    @endif
                    {!! $errors->first('song', '<p class="help-block">:message</p>') !!}
                </div>

                <!-- style="{{ isset($getArtist)?'':'display: none' }}" -->
                <div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
                    <label for="song" class="control-label">Select Artist</label>

                    <select class="form-control" id="artist_list" name="selectedArtist[]">
                        <option value="">Select Artist</option>

                        @foreach($artists as $key => $val)
                        @if(isset($song))
                        <option value="{{ $val->id}}" 
                            <?php if($val->id==$song->artist_id) echo  'selected'; ?> >{{$val->name}}
                        </option>
                        @else
                        <option value="{{ $val->id}}">{{$val->name}}</option>
                        @endif
                        @endforeach
                    </select>
                </div>
        <?php
    }
    ?>



    <!-- style="{{ isset($getAlbum)?'':'display: none' }}" -->
    <div class="form-group {{ $errors->has('Album') ? 'has-error' : ''}}">
        <label for="song" class="control-label">Select Album</label>
        <select class="form-control" id="album_list" name="selectedAlbum">
            <option value="">Select Category</option>
            @foreach($albums as $key=>$val)
            @if(isset($song))
            <option value="{{ $val->id}}"  val-id="{{ $val->id }}" 
                {{ ($val->id==$song->cat_id) ? 'selected' : '' }} >{{$val->name}}
            </option>
            @else
            <option value="{{ $val->id}}">{{$val->name}}</option>
            @endif
            @endforeach
        </select>
    </div>



   
    <span class="btn btn-info sm-btn" onclick="addArtist()">Add Artist</span>

    <section id="Appends">

    </section>


    <div class="form-group">
        <label>Free </label>
        <input type="radio" name="plans"  placeholder="Free" value="0" @if(@$song->free_premium==0) {{'checked'}} @endif>
        <label>Premium</label>
        <input type="radio" name="plans" placeholder="Premium" value="1"  @if(@$song->free_premium==1) {{'checked'}} @endif>
    </div>
 


</br>
<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>

<style type="text/css">
    .card .icon{
        display: inline !important;
        transform: none !important;
    }
</style>
<script type="text/javascript">
    function  addArtist(){
        $('#Appends').append('<div class="form-group"><label for="song" class="control-label">Select Artist</label><select class="form-control" id="artist_list" name="selectedArtist[]"><option value="">Select Artist</option> @foreach($artists as $key => $val) @if(isset($song))<option value="{{ $val->id}}">{{$val->name}} </option>@else <option value="{{ $val->id}}">{{$val->name}}</option> @endif @endforeach </select></div><div class="form-group"><label for="song" class="control-label"><input class="form-control" name="song[]" type="file" id="song" value="{{ $song->affirmation_images or ''}}" {{ !empty($song->affirmation_images)?'':'required' }} /></div>')
    }
</script>
